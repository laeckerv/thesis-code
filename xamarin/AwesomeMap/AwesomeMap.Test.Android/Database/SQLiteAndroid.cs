﻿using System;
using System.IO;
using AwesomeMap.Database;
using SQLite.Net;
using SQLite.Net.Platform.XamarinAndroid;

namespace AwesomeMap.Droid.Database
{
	public class SQLiteAndroid : ISQLite
	{
		public SQLiteConnection GetConnection (string fileName)
		{
			var path = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			return new SQLiteConnection (new SQLitePlatformAndroid (), Path.Combine (path, fileName));
		}
	}
}

