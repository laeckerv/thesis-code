﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AwesomeMap.Database;
using AwesomeMap.Droid.Database;
using AwesomeMap.Helpers;
using AwesomeMap.Models.Database;
using AwesomeMap.Utils;
using GalaSoft.MvvmLight.Ioc;
using NUnit.Framework;

namespace AwesomeMap.Tests.Android

{

	[TestFixture]
	public class WebserviceTest
	{
		static bool firstInit = false;
		public Location MyTestLocation = null;
		public int SearchRadius;

		[SetUp]
		public void Setup ()
		{
			if (firstInit) {
				var con = new SQLiteAndroid ().GetConnection ("db.db3");
				con?.DropTable<Holiday> ();
				con?.DropTable<PetrolStation> ();
				con?.DropTable<Location> ();
				con.Close ();
				Settings.CurrentDbSchemaVersion = -1;
				firstInit = false;
			}

			SimpleIoc.Default.Register<ILogger, Logger> ();
			SimpleIoc.Default.Register<ISQLite, SQLiteAndroid> ();

			MyTestLocation = new Location () {
				Latitude = 47.672133f,
				Longitude = 9.15458547f,
			};
			SearchRadius = 5;
		}

		[Test]
		public void A_StoreHolidaysFromWebservice ()
		{
			Console.Out.WriteLine ();
			Task.Run (async () => {
				await ServerConnection.UpdateHolidays ();
			}).GetAwaiter ().GetResult ();
		}

		[Test]
		public void B_GetHolidaysFromLocalDatabase ()
		{
			Console.Out.WriteLine ();
			Task.Run (async () => {
				using (var db = new Database.Database ()) {
					var items = await db.GetHolidays ();
					Assert.NotNull (items);
					Assert.AreEqual (items.Count, 46);
					foreach (var item in items) {
						Console.Out.WriteLine ($"{item.Date.ToString ("dd.MM.yyyy")}: \t {item.Name.PadRight (15)} \t {item?.FederalStatesAsString}");
					}
				}
			}).GetAwaiter ().GetResult ();
		}

		[Test]
		public void C_StoreAppParametersFromWebservice ()
		{
			Console.Out.WriteLine ();
			Task.Run (async () => {
				await ServerConnection.UpdateAppParameters ();
			}).GetAwaiter ().GetResult ();
		}

		[Test]
		public void D_GetAppParametersFromLocalDatabase ()
		{
			Console.Out.WriteLine ();
			Console.Out.WriteLine ($"RefreshPeriodDynamic: {Settings.RefreshPeriodPrices}");
			Console.Out.WriteLine ($"RefreshPeriodHolidays: {Settings.RefreshPeriodHolidays}");
			Console.Out.WriteLine ($"RefreshPeriodPetrolStation: {Settings.RefreshPeriodPetrolStation}");
			Console.Out.WriteLine ($"TrackUserPeriod: {Settings.RefreshPeriodTrackUser}");
		}

		[Test]
		public void E_GetVersionTime ()
		{
			Console.Out.WriteLine ();

			using (var db = new Database.Database ()) {
				Console.Out.WriteLine (db.GetLatestVersionTime ());
			}
		}

		/*
		[Test]
		public void F_StorePetrolStationsFromWebservice ()
		{
			Console.Out.WriteLine ();
			Task.Run (async () => {
				await ServerConnection.UpdatePetrolStations ();
			}).GetAwaiter ().GetResult ();
		}

		[Test]
		public void G_GetPetrolStationsFromLocalDatabase ()
		{
			Console.Out.WriteLine ();
			Task.Run (async () => {
				using (var db = new Database.Database ()) {
					var items = await db.GetPetrolStations ();
					Assert.NotNull (items);
					foreach (var item in items) {
						Console.Out.WriteLine ($"{item.Brand} {item.Street} {item.HouseNumber} {item.FederalState} {item.PostCode} - ({item.Loc?.Latitude}/{item.Loc?.Longitude}) - VersionTime:{item.VersionTime.ToString ("dd.MM.yyyy hh:mm")}");
					}
				}
			}).GetAwaiter ().GetResult ();
		}
		*/


		[Test]
		public void H_GetPetrolStationsInRadius ()
		{
			Console.Out.WriteLine ();

			Task.Run (async () => {
				List<PetrolStation> petrolStations = null;

				using (var db = new Database.Database ()) {
					petrolStations = await db.GetPetrolStationsInRegion (MyTestLocation, SearchRadius);
					Assert.NotNull (petrolStations);
					foreach (var item in petrolStations) {
						Console.Out.WriteLine ($"{item.Brand} {item.Street} {item.HouseNumber} {item.FederalState} {item.PostCode} - ({item.Loc?.Latitude}/{item.Loc?.Longitude}) - VersionTime:{item.VersionTime.ToString ("dd.MM.yyyy hh:mm")}");
					}
				}
			}).GetAwaiter ().GetResult ();
		}


		[Test]
		public void I_UpdatePetrolStationPricesInRadius ()
		{
			Console.Out.WriteLine ();

			Task.Run (async () => {
				List<PetrolStation> petrolStationsOld = null;

				using (var db = new Database.Database ()) {
					petrolStationsOld = await db.GetPetrolStationsInRegion (MyTestLocation, SearchRadius);
					Assert.NotNull (petrolStationsOld);
				}

				await ServerConnection.UpdatePriceForPetrolStations (petrolStationsOld);
				foreach (var item in petrolStationsOld) {
					Console.Out.WriteLine ($"{item.Id}:{item.Brand} - ({item.Loc?.Latitude}/{item.Loc?.Longitude}) - PriceE5: {item.PriceE5} - PriceE10: {item.PriceE10} - PriceDiesel: {item.PriceDiesel}");
					Console.Out.WriteLine ("##############################");
				}
			}).GetAwaiter ().GetResult ();

		}

		[TearDown]
		public void TearDown ()
		{
			SimpleIoc.Default.Reset ();
		}
	}
}

