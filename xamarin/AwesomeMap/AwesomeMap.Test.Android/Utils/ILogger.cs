﻿using System;
namespace AwesomeMap.Utils
{
	class Logger : ILogger
	{
		public void Debug (string msg)
		{
			Console.WriteLine (DateTime.Now.ToString ("u") + " [DEBUG]: " + msg);
		}

		public void Info (string msg)
		{
			Console.WriteLine (DateTime.Now.ToString ("u") + " [INFO]: " + msg);
		}

		public void Warn (string msg)
		{
			Console.WriteLine (DateTime.Now.ToString ("u") + " [WARN]: " + msg);
		}

		public void Error (String msg, Exception e = null)
		{
			if (e == null) {
				Console.WriteLine (DateTime.Now.ToString ("u") + " [ERROR]: " + msg);
			} else {
				Console.WriteLine (DateTime.Now.ToString ("u") + " [ERROR]: " + msg + " | err msg: " + e.Message);
				Console.WriteLine (e);
			}
		}

		public void FatalError (String msg, Exception e = null)
		{
#if DEBUG
			if (e == null) {
				Console.WriteLine (DateTime.Now.ToString ("u") + " [FATAL ERROR]: " + msg);
			} else {
				Console.WriteLine (DateTime.Now.ToString ("u") + " [FATAL ERROR]: " + msg + " | err msg: " + e.Message);
				Console.WriteLine (e);
			}
#elif HOCKEY_CRASH
			HockeyApp.TraceWriter.WriteTrace (e, false);
#endif
		}
	}
}

