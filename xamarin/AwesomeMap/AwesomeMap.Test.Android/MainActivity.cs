﻿using System;
using System.IO;
using System.Reflection;
using Android.App;
using Android.OS;
using Xamarin.Android.NUnitLite;

namespace AwesomeMap.Test.Android
{
	[Activity (Label = "AwesomeMap.Test.Android", MainLauncher = true)]
	public class MainActivity : TestSuiteActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			AddTest (Assembly.GetExecutingAssembly ());

			// Once you called base.OnCreate(), you cannot add more assemblies.
			base.OnCreate (bundle);

			// Copy DB
			var docFolder = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
			var dbFile = Path.Combine (docFolder, "db.db3");
			if (!File.Exists (dbFile)) {
				var s = Resources.OpenRawResource (Resource.Raw.db);
				FileStream writeStream = new FileStream (dbFile, FileMode.OpenOrCreate, FileAccess.Write);
				ReadWriteStream (s, writeStream);
			}
		}

		private void ReadWriteStream (Stream readStream, Stream writeStream)
		{
			int Length = 256;
			Byte [] buffer = new Byte [Length];
			int bytesRead = readStream.Read (buffer, 0, Length);
			// write the required bytes
			while (bytesRead > 0) {
				writeStream.Write (buffer, 0, bytesRead);
				bytesRead = readStream.Read (buffer, 0, Length);
			}
			readStream.Close ();
			writeStream.Close ();
		}
	}
}

