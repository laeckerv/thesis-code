﻿using System;
using System.IO;
using AwesomeMap.Database;
using SQLite.Net;
using SQLite.Net.Async;
using SQLite.Net.Platform.XamarinIOS;

namespace AwesomeMap.iOS.Helpers
{
	public class SQLite : ISQLite
	{
		public SQLiteAsyncConnection GetConnection (string fileName)
		{
			var path = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), fileName);
			var platform = new SQLitePlatformIOS ();
			var connectionWithLock = new SQLiteConnectionWithLock (
							 platform,
							 new SQLiteConnectionString (path, true)
			);

			return new SQLiteAsyncConnection (() => connectionWithLock);
		}
	}
}

