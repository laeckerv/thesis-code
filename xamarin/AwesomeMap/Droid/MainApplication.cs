﻿using System;
using System.IO;
using Android.App;
using Android.Runtime;
using AwesomeMap.Database;
using AwesomeMap.Droid.Database;
using AwesomeMap.Utils;
using GalaSoft.MvvmLight.Ioc;

namespace AwesomeMap.Droid
{
	[Application]
	public class MainApplication : Application
	{
		public MainApplication (IntPtr handle, JniHandleOwnership ownerShip) : base(handle, ownerShip)
    	{
		}

		public override void OnCreate ()
		{
			base.OnCreate ();

			// Register IOCs
			SimpleIoc.Default.Register<ILogger, Logger> ();
			SimpleIoc.Default.Register<ISQLite, SQLiteAndroid> ();

			LoggingHandler.Instance.LogLevel = LoggingHandler.LogLevels.debug;

			// Copy prepopulated database if not already done.
			var docFolder = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			var dbFile = Path.Combine (docFolder, "db.db3");
			if (!File.Exists (dbFile)) {
				var s = Resources.OpenRawResource (Resource.Raw.db);
				var writeStream = new FileStream (dbFile, FileMode.OpenOrCreate, FileAccess.Write);
				ReadWriteStream (s, writeStream);
			}
		}

		private void ReadWriteStream (Stream readStream, Stream writeStream)
		{
			int Length = 256;
			byte [] buffer = new Byte [Length];
			int bytesRead = readStream.Read (buffer, 0, Length);
			// write the required bytes
			while (bytesRead > 0) {
				writeStream.Write (buffer, 0, bytesRead);
				bytesRead = readStream.Read (buffer, 0, Length);
			}
			readStream.Close ();
			writeStream.Close ();
		}
	}
}

