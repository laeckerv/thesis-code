﻿using System;
using System.IO;
using AwesomeMap.Database;
using SQLite.Net;
using SQLite.Net.Async;
using SQLite.Net.Platform.XamarinAndroid;

namespace AwesomeMap.Droid.Database
{
	public class SQLiteAndroid : ISQLite
	{
		public SQLiteAsyncConnection GetConnection (string fileName)
		{
			var path = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), fileName);
			var platform = new SQLitePlatformAndroid ();

			var connectionWithLock = new SQLiteConnectionWithLock (
										 platform,
										 new SQLiteConnectionString (path, true)
			);

			return new SQLiteAsyncConnection (() => connectionWithLock);

		}
	}
}

