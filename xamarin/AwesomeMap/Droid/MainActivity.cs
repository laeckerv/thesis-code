﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Graphics;
using Android.Locations;
using Android.OS;
using Android.Views;
using Android.Widget;
using AwesomeMap.Database;
using AwesomeMap.Droid.Models;
using AwesomeMap.Models.Database;
using AwesomeMap.Utils;
using Com.Google.Maps.Android.Clustering;
using SQLite.Net.Attributes;

namespace AwesomeMap.Droid
{
	[Activity (Label = "AwesomeMap", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity, IOnMapReadyCallback
	{
		private GoogleMap map;
		private bool first = true;
		private ClusterManager clusterMananger = null;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);


			MapFragment mapFrag = (MapFragment)FragmentManager.FindFragmentById (Resource.Id.map);
			mapFrag.GetMapAsync (this);

		}

		protected override void OnResume ()
		{
			base.OnResume ();

			Task.Run (async () => {
				await ServerConnection.UpdateMetadata ();
			});
		}

		public void OnMapReady (GoogleMap googleMap)
		{
			map = googleMap;
			map.MyLocationEnabled = true;
			map.MyLocationChange += async (_, e) => {
				await OnLocationChanged (e.Location);
			};

			//map.CameraChange += (o, e) => OnCameraMoved (o, e);
			map.MapType = GoogleMap.MapTypeNormal;
			clusterMananger = new ClusterManager (this, map);
			map.SetOnCameraChangeListener (clusterMananger);
			map.SetOnMarkerClickListener (clusterMananger);
		}

		private BitmapDescriptor GetCustomBitmapDescriptor (string brand, string price)
		{
			View mapMarker = LayoutInflater.Inflate (Resource.Layout.map_marker, null);
			TextView mapMarkerBrandText = mapMarker.FindViewById<TextView> (Resource.Id.map_marker_brand);
			TextView mapMarkerPriceText = mapMarker.FindViewById<TextView> (Resource.Id.map_marker_price);
			TextView mapMarkerPriceTextSmall = mapMarker.FindViewById<TextView> (Resource.Id.map_marker_price_smallNumber);

			mapMarkerBrandText.SetText (brand, TextView.BufferType.Normal);
			if (price.Length == 5) {
				mapMarkerPriceText.SetText (price.Remove (3, 1), TextView.BufferType.Normal);
				mapMarkerPriceTextSmall.SetText (price.Remove (0, 4), TextView.BufferType.Normal);
			} else {
				mapMarkerPriceText.SetText ("n/a", TextView.BufferType.Normal);
			}

			mapMarker.Measure (View.MeasureSpec.MakeMeasureSpec (0, MeasureSpecMode.Unspecified),
							   View.MeasureSpec.MakeMeasureSpec (0, MeasureSpecMode.Unspecified));

			mapMarker.Layout (0, 0, mapMarker.MeasuredWidth, mapMarker.MeasuredHeight);

			using (var mapMarkerBitmap = Bitmap.CreateBitmap (mapMarker.MeasuredWidth, mapMarker.MeasuredHeight, Bitmap.Config.Argb8888)) {

				using (var canvas = new Canvas (mapMarkerBitmap)) {
					mapMarker.Draw (canvas);
				}

				return BitmapDescriptorFactory.FromBitmap (mapMarkerBitmap);
			}
		}

		public async Task OnLocationChanged (Android.Locations.Location location)
		{
			if (first) {
				first = false;
				List<PetrolStation> petrolStations = null;
				using (var db = new AwesomeMap.Database.Database ()) {
					//petrolStations = await db.GetPetrolStations ();

					petrolStations = await db.GetPetrolStationsInRegion (
					new AwesomeMap.Models.Database.Location () {
						Latitude = (float)location.Latitude,
						Longitude = (float)location.Longitude
					}, 50);

				}

				int i = 0;

				if (petrolStations != null) {
					foreach (var item in petrolStations) {
						LoggingHandler.Instance.Debug ($"Adding marker no: {i++}");
						map.AddMarker (new MarkerOptions ()
							.SetPosition (new LatLng (item.Loc.Latitude, item.Loc.Longitude))
							.SetIcon (GetCustomBitmapDescriptor (item.Brand, item.PriceE5.ToString ()))
						);
						//clusterMananger.AddItem (new ServiceClusterItem (item.Loc.Latitude, item.Loc.Longitude));
					}
				}

				map.MoveCamera (CameraUpdateFactory.NewLatLngZoom (new LatLng (location.Latitude, location.Longitude), 13));
			}
		}

		public void OnCameraMoved (object o, GoogleMap.CameraChangeEventArgs e)
		{
			/*
			List<PetrolStation> petrolStations = null;
			map.Clear ();

			using (var db = new AwesomeMap.Database.Database ()) {
				petrolStations = db.GetPetrolStationsInRegion (
					new Models.Database.Location () {
						Latitude = (float)e.Position.Target.Latitude,
						Longitude = (float)e.Position.Target.Longitude
					},
					5);
			}

			if (petrolStations != null) {
				foreach (var item in petrolStations) {
					map.AddMarker (new MarkerOptions ()
								   .SetPosition (new LatLng (item.Loc.Latitude, item.Loc.Longitude))
								   .SetIcon (GetCustomBitmapDescriptor (item.Brand, item.PriceE5.ToString ()))
				  	);
				}
			}
			*/

		}
	}
}


