namespace AwesomeMap.Models.Json
{
	public class AppParameterJSON
	{
		public int RefreshPeriodDynamic { get; set; }
		public int RefreshPeriodHolidays { get; set; }
		public int RefreshPeriodPetrolStation { get; set; }
		public int TrackUserPeriod { get; set; }
	}
}
