using System;
using System.Collections.Generic;
using AwesomeMap.Models.Database;

namespace AwesomeMap.Models.Json
{

	public class HolidayAsJson : Holiday
	{
		public Holiday toHoliday (bool continuousHoliday = false)
		{
			this.Date = new DateTime (this.Date.Year, this.Date.Month, this.Date.Day, 0,0,0, DateTimeKind.Utc);
			if (continuousHoliday && this.Date.Date < DateTime.Now.Date) {
				this.Date = this.Date.Date.AddYears (1);
			}

			if (this.FederalStates != null) {
				this.FederalStatesAsString = string.Join (",", this.FederalStates.ToArray ());
			}

			return new Holiday () {
				Date = this.Date,
				Name = this.Name,
				FederalStatesAsString = this.FederalStatesAsString
			};
		}

		public Holiday toHoliday (DateTime date)
		{
			this.Date = date;
			return this.toHoliday ();
		}

		public List<DateTime> Dates { get; set; }
		public List<string> FederalStates { get; set; }
	}
}
