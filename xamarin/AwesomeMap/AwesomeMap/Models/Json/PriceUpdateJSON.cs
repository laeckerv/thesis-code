﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace AwesomeMap.Models.Json
{
	public class FuelPriceJSON
	{
		public DateTime DateOfPrice { get; set; }

		[JsonProperty ("fuelPrice")]
		public double Price { get; set; }

		[JsonProperty ("type")]
		public string Type { get; set; }
	}

	public class PriceUpdateJSON
	{
		[JsonProperty ("petrolStationID")]
		public string PetrolStationID { get; set; }

		[JsonProperty ("fuelPrices")]
		public IList<FuelPriceJSON> FuelPrices { get; set; }
	}

}

