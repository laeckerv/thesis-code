using SQLiteNetExtensions.Attributes;
using SQLite.Net.Attributes;
using System;

namespace AwesomeMap.Models.Database
{
	public class Location
	{
		private const double EARTH_RADIUS_KM = 6371;

		[PrimaryKey,AutoIncrement]
		public int Id { get; set; }

		[ForeignKey (typeof (PetrolStation))]
		public string PetrolStationId { get; set; }

		public float Latitude { get; set; }
		public float Longitude { get; set; }

		public bool IsInDistanceOf(Location targetLocation, double maxDistanceKM) {
			return GetDistanceKM (targetLocation) <= maxDistanceKM;
		}

		public double GetDistanceKM (Location targetLocation)
		{
			double dLat = ToRad (targetLocation.Latitude - this.Latitude);
			double dLon = ToRad (targetLocation.Longitude - this.Longitude);

			double a = Math.Pow (Math.Sin (dLat / 2), 2) +
					   Math.Cos (ToRad (this.Latitude)) * Math.Cos (ToRad (targetLocation.Latitude)) *
					   Math.Pow (Math.Sin (dLon / 2), 2);

			double c = 2 * Math.Atan2 (Math.Sqrt (a), Math.Sqrt (1 - a));

			return EARTH_RADIUS_KM * c;
		}

		private static double ToRad (double input)
		{
			return input * (Math.PI / 180);
		}
	}
}
