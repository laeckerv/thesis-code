﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace AwesomeMap.Models.Database
{
	public class PetrolStation
	{
		[PrimaryKey]
		public string Id { get; set;}
		public float Distance { get; set; }

		[OneToOne (CascadeOperations = CascadeOperation.All), JsonProperty ("Location")]
		public Location Loc { get; set; }

		public double PriceDiesel { get; set; }
		public double PriceE10 { get; set; }
		public double PriceE5 { get; set; }
		public DateTime PriceLastUpdate { get; set; }
		public DateTime VersionTime { get; set; }

		public string Brand { get; set; }
		public string Name { get; set; }
		public string Street { get; set; }
		public string HouseNumber { get; set; }
		public string Place { get; set; }
		public string PostCode { get; set; }
		public string FederalState { get; set; }
		public bool Deleted { get; set; }

		[OneToMany(CascadeOperations = CascadeOperation.All)]
		public List<OpeningHour> OpeningHours { get; set; }
	}


}

