﻿using SQLite.Net.Async;

namespace AwesomeMap.Database
{
	public interface ISQLite
	{
		SQLiteAsyncConnection GetConnection (string fileName);
	}
}


