﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AwesomeMap.Helpers;
using AwesomeMap.Models.Database;
using AwesomeMap.Utils;
using GalaSoft.MvvmLight.Ioc;
using SQLite.Net.Async;

namespace AwesomeMap.Database
{
	public class DbConnectionManager
	{
		public const int DB_SCHEMA_VERSION = 0;
		private const string DB_FILENAME = "db.db3";
		private static object locker = new object ();
		private HashSet<SQLiteAsyncConnection> idleConnections = new HashSet<SQLiteAsyncConnection> ();

		#region Singleton
		private static volatile DbConnectionManager instance;
		private static object syncRoot = new Object ();
		public static DbConnectionManager Instance {
			get {
				if (instance == null) {
					lock (syncRoot) {
						if (instance == null)
							instance = new DbConnectionManager ();
					}
				}

				return instance;
			}
		}
		#endregion

		private DbConnectionManager ()
		{
			Task.Run (async () => {
				await Initialize ();
			});
		}

		private async Task Initialize ()
		{
			var database = GetConnection ();

			try {
				if (Settings.CurrentDbSchemaVersion < DB_SCHEMA_VERSION) {
					LoggingHandler.Instance.Info ($"Recreate database tables as version schema changed from {Settings.CurrentDbSchemaVersion} to {DB_SCHEMA_VERSION}");
					await ResetDB (database, true);
				}
			} catch (Exception e) {
				LoggingHandler.Instance.Info ("Recreate database tables, because an error occurred: " + e.Message);
				await ResetDB (database, true);
			} finally {
				FreeConnection (database);
			}
		}

		private async Task ResetDB (SQLiteAsyncConnection database, bool fullWipe)
		{
			await database.DropTableAsync<Holiday> ();
			await database.DropTableAsync<Location> ();
			await database.DropTableAsync<OpeningHour> ();
			await database.DropTableAsync<PetrolStation> ();

			if (fullWipe) {
				//database.DropTable<KeyValueTable> ();
			}

			await database.CreateTablesAsync (
				typeof (Holiday), typeof (PetrolStation),
				typeof (Location), typeof (OpeningHour)
			);

			Settings.CurrentDbSchemaVersion = DB_SCHEMA_VERSION;
		}

		public SQLiteAsyncConnection GetConnection ()
		{
			lock (locker) {
				if (idleConnections.Count == 0) {
					return CreateNewConnection ();
				}
				var con = idleConnections.First ();
				idleConnections.Remove (con);
				return con;
			}
		}

		public void FreeConnection (SQLiteAsyncConnection c)
		{
			lock (locker) {
				idleConnections.Add (c);
			}
		}

		private SQLiteAsyncConnection CreateNewConnection ()
		{
			if (SimpleIoc.Default.IsRegistered<ISQLite> ()) {
				var sqlite = SimpleIoc.Default.GetInstance<ISQLite> ();
				if (sqlite == null) {
					throw new DatabaseException ("There is no implementation to create a DB connection");
				}
				var database = sqlite.GetConnection (DB_FILENAME);
				if (database == null) {
					throw new DatabaseException ("Database connection could not be established");
				}
				return database;
			}
			return null;
		}

	}
}


