﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AwesomeMap.Helpers;
using AwesomeMap.Models.Database;
using AwesomeMap.Utils;
using SQLite.Net.Async;
using SQLiteNetExtensionsAsync.Extensions;
using System.Threading;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace AwesomeMap.Database
{
	public class Database : IDisposable
	{
		private readonly SQLiteAsyncConnection database = null;
		private static readonly AsyncLock locker = new AsyncLock ();

		private DbConnectionManager dbConnectionManager;

		private Database (DbConnectionManager dbConnectionManager)
		{
			this.dbConnectionManager = dbConnectionManager;
		}

		public Database () : this (DbConnectionManager.Instance)
		{
			database = dbConnectionManager.GetConnection ();
		}

		public void Dispose ()
		{
			if (database != null) {
				DbConnectionManager.Instance.FreeConnection (database);
			}
		}

		public async Task SaveHolidays (List<Holiday> holidayList)
		{
			if (database != null) {
				LoggingHandler.Instance.Debug ("Delete all holidays from database.");
				await database.DeleteAllAsync<Holiday> ();
				using (await locker.LockAsync ()) {
					await database.InsertAllAsync (holidayList);
				}
			}
		}

		public async Task<List<Holiday>> GetHolidays ()
		{
			if (database != null) {
				return await database.GetAllWithChildrenAsync<Holiday> ();
			}
			return null;
		}

		public async Task SavePetrolStations (List<PetrolStation> petrolStations)
		{
			if (database != null) {
				using (await locker.LockAsync ()) {
					await database.InsertOrReplaceAllWithChildrenAsync (petrolStations);
				}
			}
		}

		public async Task SavePetrolStation (PetrolStation petrolStation)
		{
			if (database != null) {
				using (await locker.LockAsync ()) {
					await database.InsertOrReplaceWithChildrenAsync (petrolStation);
				}
			}
		}

		public async Task<List<PetrolStation>> GetPetrolStations (bool includeDeleted = false)
		{
			if (database != null) {
				var result = await database.GetAllWithChildrenAsync<PetrolStation> ();
				var t = result.Where (x => x.Deleted == includeDeleted);
				return t.ToList ();
			}
			return null;
		}

		public async Task<List<PetrolStation>> GetPetrolStationsInRegion (Location location, float distance, bool includeDeleted = false)
		{
			if (database != null) {
				var ret = new List<PetrolStation> ();
				var limit = 900;
				var petrolStationIds = (await database.Table<Location> ().ToListAsync ()).Where (l => l.IsInDistanceOf (location, distance)).Select (l => l.PetrolStationId);
				foreach (var primaryKeys in Split (petrolStationIds.ToList (), limit)) {
					ret.AddRange (await database.GetAllWithChildrenAsync<PetrolStation> (p => p.Deleted == includeDeleted && primaryKeys.Contains (p.Id)));
				}
				return ret;
			}
			return null;
		}

		static List<List<T>> Split<T> (List<T> items, int sliceSize = 30)
		{
			var list = new List<List<T>> ();
			for (int i = 0; i < items.Count; i += sliceSize)
				list.Add (items.GetRange (i, Math.Min (sliceSize, items.Count - i)));
			return list;
		}

		public async Task ClearPrices (List<PetrolStation> petrolStations)
		{
			if (database != null) {
				foreach (var item in petrolStations) {
					item.PriceE5 = 0;
					item.PriceE10 = 0;
					item.PriceDiesel = 0;
				}
				using (await locker.LockAsync ()) {
					await database.InsertOrReplaceAllWithChildrenAsync (petrolStations);
				}
			}
		}

		public async Task<DateTime> GetLatestVersionTime ()
		{
			if (database != null) {
				var result = await database.QueryAsync<PetrolStation> ("SELECT * FROM PetrolStation ORDER BY VersionTime DESC LIMIT 1");
				if (result.Count > 0) {
					return result.First ().VersionTime;
				}
			}
			return Settings.PetrolStationVersionTime;
		}
	}
}

