﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AwesomeMap.Helpers;
using AwesomeMap.Models.Database;
using AwesomeMap.Models.Json;
using AwesomeMap.Utils;
using Newtonsoft.Json;

namespace AwesomeMap.Database
{

	public sealed class ServerConnection
	{
		#region Singleton 

		static volatile ServerConnection instance;
		static object syncRoot = new Object ();
		ServerConnection () { }
		public static ServerConnection Instance {
			get {
				if (instance == null) {
					lock (syncRoot) {
						if (instance == null)
							instance = new ServerConnection ();
					}
				}

				return instance;
			}
		}

		#endregion

		static string baseUrl = "https://api.spritradarapp.de/v1";
		static readonly string petrolStationPricesURL = $"{baseUrl}/petrolstations/dynamic/";
		static readonly string petrolStationUpdateURL = $"{baseUrl}/petrolstations/changes?versiontime=";
		static readonly string appParametersURL = $"{baseUrl}/info/appParameters/";
		static readonly string holidaysURL = $"{baseUrl}/info/holidays/";
		static readonly string userURL = $"{baseUrl}/user";
		static readonly string user = "spr1.0.0";
		static readonly string pwd = "zdBAF7jfvDwR2HrBYFjoxtEpcYfAdyhYi";
		static readonly int chunkSize = 50;
		static NetworkCredential credentials = new NetworkCredential (user, pwd);

		static string reportProblemForPetrolStationURL (PetrolStation petrolStation)
		{
			return $"{baseUrl}/petrolstations/{petrolStation.Id}/complaint";
		}

		static string trackUsersPetrolStationURL (string userServerID)
		{
			return $"{userURL}/{userServerID}/track";
		}

		static async Task<string> GetJsonContent (string url)
		{
			var request = WebRequest.CreateHttp (url);
			request.ContentType = "application/json";
			request.Method = "GET";
			request.Credentials = credentials;

			var requestTask = Task.Factory.FromAsync (request.BeginGetResponse, request.EndGetResponse, request);
			using (var response = await requestTask) {
				using (StreamReader reader = new StreamReader (response.GetResponseStream ())) {
					return reader.ReadToEnd ();
				}
			}
		}

		public static async Task UpdateMetadata (bool includePetrolstation = true)
		{
			await UpdateAppParameters ();
			await UpdateHolidays ();
			if (includePetrolstation)
				await UpdatePetrolStations ();
		}

		public static async Task UpdateHolidays ()
		{
			if (DateTime.Now > Settings.LastUpdateHolidays.AddSeconds (Settings.RefreshPeriodHolidays)) {
				var holidayList = new List<Holiday> ();
				var content = await GetJsonContent (holidaysURL);

				if (!string.IsNullOrWhiteSpace (content)) {
					var contenAsObjects = JsonConvert.DeserializeObject<List<HolidayAsJson>> (content);

					foreach (var item in contenAsObjects) {
						if (item.Dates != null) {
							foreach (var date in item.Dates) {
								holidayList.Add (item.toHoliday (date));
							}
						} else {
							holidayList.Add (item.toHoliday (true));
						}
					}

					using (var db = new Database ()) {
						await db.SaveHolidays (holidayList);
						Settings.LastUpdateHolidays = DateTime.Now;
					}
				}
			} else {
				LoggingHandler.Instance.Debug ($"Holidays are up to date ({Settings.LastUpdateHolidays.ToString ("yyyy.MM.dd hh:mm:ss")}) - no update needed");
			}
		}

		public static async Task UpdateAppParameters ()
		{
			if (DateTime.Now > Settings.LastUpdateAppParameter.AddSeconds (Settings.RefreshPeriodAppParameter)) {
				var content = await GetJsonContent (appParametersURL);
				if (!string.IsNullOrWhiteSpace (content)) {
					var appParameters = JsonConvert.DeserializeObject<AppParameterJSON> (content);
					if (appParameters != null) {
						Settings.RefreshPeriodPrices = appParameters.RefreshPeriodDynamic;
						Settings.RefreshPeriodHolidays = appParameters.RefreshPeriodHolidays;
						Settings.RefreshPeriodPetrolStation = appParameters.RefreshPeriodPetrolStation;
						Settings.RefreshPeriodTrackUser = appParameters.TrackUserPeriod;
						Settings.LastUpdateAppParameter = DateTime.Now;
					}
				}
			} else {
				LoggingHandler.Instance.Debug ($"AppParameters are up to date ({Settings.LastUpdateAppParameter.ToString ("yyyy.MM.dd hh:mm:ss")}) - no update needed");
			}
		}

		public static async Task UpdatePetrolStations ()
		{
			if (Settings.LastUpdatePetrolStations == DateTime.MinValue) {
				using (var db = new Database ()) {
					Settings.PetrolStationVersionTime = await db.GetLatestVersionTime ();
					Settings.LastUpdatePetrolStations = Settings.PetrolStationVersionTime;
				}
			}

			if (DateTime.Now > Settings.LastUpdatePetrolStations.AddSeconds (Settings.RefreshPeriodPetrolStation)) {
				var versionTimeString = $"{Settings.PetrolStationVersionTime.ToString ("yyyy-MM-ddThh:mm:ss")}{Settings.PetrolStationVersionTime.ToString ("zzz").Replace ("+", "%2b").Replace (":", "")}";
				var content = await GetJsonContent ($"{petrolStationUpdateURL}{versionTimeString}");
				if (!string.IsNullOrWhiteSpace (content)) {
					using (var db = new Database ()) {
						var contentAsObject = JsonConvert.DeserializeObject<List<PetrolStationAsJSON>> (content);
						await db.SavePetrolStations (contentAsObject.Select (x => x.toPetrolStation ()).ToList ());
						Settings.PetrolStationVersionTime = await db.GetLatestVersionTime ();
						Settings.LastUpdatePetrolStations = DateTime.Now;
					}
				}
			} else {
				LoggingHandler.Instance.Debug ($"Petrolstations are up to date ({Settings.LastUpdatePetrolStations.ToString ("yyyy.MM.dd hh:mm:ss")}) - no update needed");
			}
		}

		public static async Task UpdatePriceForPetrolStation (PetrolStation petrolStation)
		{
			var content = await GetJsonContent ($"{petrolStationPricesURL}{petrolStation.Id}");
			if (!string.IsNullOrWhiteSpace (content)) {
				using (var db = new Database ()) {
					var contentAsObject = JsonConvert.DeserializeObject<List<PriceUpdateJSON>> (content);
					foreach (var item in contentAsObject) {
						if (petrolStation.Id == item.PetrolStationID) {
							foreach (var fuelPrice in item.FuelPrices) {
								switch (fuelPrice.Type) {
								case "E5":
									petrolStation.PriceE5 = fuelPrice.Price;
									break;
								case "E10":
									petrolStation.PriceE10 = fuelPrice.Price;
									break;
								case "DIESEL":
									petrolStation.PriceDiesel = fuelPrice.Price;
									break;
								default:
									break;
								}
								petrolStation.PriceLastUpdate = fuelPrice.DateOfPrice;
							}
							await db.SavePetrolStation (petrolStation);
						}
					}
				}
			}
		}

		public static async Task UpdatePriceForPetrolStations (List<PetrolStation> petrolStations)
		{
			for (int i = 0; i < Math.Ceiling (petrolStations.Count / (chunkSize * 1.0)); i++) {
				var chunkOfPetrolStations = petrolStations.Skip ((i * chunkSize)).Take (chunkSize);
				var content = await GetJsonContent ($"{petrolStationPricesURL}{string.Join (",", chunkOfPetrolStations.Select (x => x.Id).ToArray ())}");

				if (!string.IsNullOrWhiteSpace (content)) {
					var toSave = new List<PetrolStation> ();
					using (var db = new Database ()) {
						var contentAsObject = JsonConvert.DeserializeObject<List<PriceUpdateJSON>> (content);
						foreach (var item in contentAsObject) {
							if (item.FuelPrices != null) {
								var petrolStation = chunkOfPetrolStations.First (x => x.Id == item.PetrolStationID);
								if (petrolStation != null) {
									foreach (var fuelPrice in item.FuelPrices) {
										switch (fuelPrice.Type) {
										case "E5":
											petrolStation.PriceE5 = fuelPrice.Price;
											break;
										case "E10":
											petrolStation.PriceE10 = fuelPrice.Price;
											break;
										case "DIESEL":
											petrolStation.PriceDiesel = fuelPrice.Price;
											break;
										default:
											break;
										}

										/* TODO: Once webservice offers correct dates we should store them in the data base.
										if (fuelPrice.DateOfPrice != DateTime.MinValue)
											petrolStation.PriceLastUpdate = fuelPrice.DateOfPrice;
										*/
									}
									toSave.Add (petrolStation);
								}
							}
						}
						await db.SavePetrolStations (toSave);
					}
				}
			}
		}
	}
}

