﻿using Xamarin.Forms;
using Xamarin.Forms.Maps;
using AwesomeMap.Models.Database;
using System.Threading.Tasks;
using AwesomeMap.Utils;
using AwesomeMap.Database;
using System.Dynamic;
using System.Reflection;
using System;
using AwesomeMap.Helpers;
using System.Linq;
using Xamarin.Forms.Xaml;
using System.Globalization;

namespace AwesomeMap
{
	public partial class AwesomeMapPage : ContentPage
	{
		static double _latitude = 47.673503;
		static double _longitude = 9.147564;
		double y;

		public AwesomeMapPage ()
		{
			InitializeComponent ();

#if DEBUG
			LoggingHandler.Instance.LogLevel = LoggingHandler.LogLevels.debug;
#endif


			var map = new PsMap (
				MapSpan.FromCenterAndRadius (
					new Position (_latitude, _longitude), Distance.FromKilometers (5)
				)) {
				IsShowingUser = true,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};


			RelativeLayout layout = new RelativeLayout ();
			var topMenu = new BoxView { Color = Color.White, Opacity = 0.9 };
			var listView = new ListView { BackgroundColor = Color.White, Opacity = 0.9 };
			listView.ItemsSource = map.VisiblePetrolStations;
			listView.ItemTemplate = new DataTemplate (typeof (PetrolStationCell));
			listView.ItemTemplate.SetBinding (TextCell.TextProperty, "Brand");
			listView.ItemTemplate.SetBinding (TextCell.DetailProperty, "Price");
			listView.RowHeight = 80;
			listView.ItemSelected += (sender, e) => {
				(sender as ListView).SelectedItem = null;
			};

			var listStack = new RelativeLayout ();
			listStack.Children.Add (new Label () { Text = "=", BackgroundColor = Color.Gray, HorizontalTextAlignment = TextAlignment.Center },
								   Constraint.Constant (0),
								   Constraint.Constant (0),
								   Constraint.RelativeToParent ((parent) => { return parent.Width; }),
								   Constraint.Constant (25));

			listStack.Children.Add (listView,
									Constraint.Constant (0),
									Constraint.Constant (25),
									Constraint.RelativeToParent ((parent) => { return parent.Width; }),
									Constraint.Constant (480)
								  );

			var panGesture = new PanGestureRecognizer ();
			panGesture.PanUpdated += (s, e) => {
				listView.HeightRequest = 120;
				switch (e.StatusType) {
				case GestureStatus.Running:
					listStack.TranslationY = Math.Min (0, Math.Max (y + e.TotalY, -400)); ;
					break;
				case GestureStatus.Completed:
					y = listStack.TranslationY < -120 ? -240 : 0;
					listStack.TranslateTo (0, y, 250, Easing.CubicInOut);
					break;
				default:
					break;
				}
			};
			listStack.GestureRecognizers.Add (panGesture);


			layout.Children.Add (map,
								 Constraint.Constant (0),
								 Constraint.Constant (0),
								 Constraint.RelativeToParent ((parent) => { return parent.Width; }),
								 Constraint.RelativeToParent ((parent) => { return parent.Height; })
								);

			layout.Children.Add (topMenu,
								 Constraint.Constant (0),
								 Constraint.Constant (0),
								 Constraint.RelativeToParent ((parent) => { return parent.Width; }),
								 Constraint.Constant (60)
								);

			layout.Children.Add (listStack,
								 Constraint.Constant (0),
								 Constraint.RelativeToParent ((parent) => {
									 return parent.Height - 105;
								 }),
								 Constraint.RelativeToParent ((parent) => { return parent.Width; }),
								 Constraint.Constant (505)
								);

			Content = layout;
		}
	}

	public class PetrolStationCell : ViewCell
	{
		public PetrolStationCell ()
		{
			Grid grid = new Grid {
				VerticalOptions = LayoutOptions.FillAndExpand,
				RowDefinitions =
				{
					new RowDefinition { Height = GridLength.Auto },
					new RowDefinition { Height = GridLength.Auto },
				},
				ColumnDefinitions =
				{
					new ColumnDefinition { Width = new GridLength(70, GridUnitType.Absolute) },
					new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
					new ColumnDefinition { Width = GridLength.Auto },
				}
			};

			// TODO: Design cell.
			var brandLabel = new Label ();
			brandLabel.SetBinding (Label.TextProperty, new Binding ("Brand"));
			brandLabel.FontSize = 10;
			brandLabel.LineBreakMode = LineBreakMode.TailTruncation;
			brandLabel.TextColor = Color.Red;

			var priceLabel = new Label ();
			priceLabel.SetBinding (Label.TextProperty, new Binding ("Price"));
			priceLabel.FontSize = 18;

			grid.Children.Add (brandLabel, 0, 0);
			grid.Children.Add (priceLabel, 0, 1);

			var addressStack = new StackLayout {
				Orientation = StackOrientation.Horizontal
			};

			var streetLabel = new Label ();
			streetLabel.SetBinding (Label.TextProperty, new Binding ("Street"));
			streetLabel.FontSize = 12;

			var numberLabel = new Label ();
			numberLabel.SetBinding (Label.TextProperty, new Binding ("HouseNumber"));
			numberLabel.FontSize = 12;

			addressStack.Children.Add (streetLabel);
			addressStack.Children.Add (numberLabel);

			var placeStack = new StackLayout {
				Orientation = StackOrientation.Horizontal
			};

			var postCodeLabel = new Label ();
			postCodeLabel.SetBinding (Label.TextProperty, new Binding ("PostCode"));
			postCodeLabel.FontSize = 12;

			var placeLabel = new Label ();
			placeLabel.SetBinding (Label.TextProperty, new Binding ("Place"));
			placeLabel.FontSize = 12;

			placeStack.Children.Add (postCodeLabel);
			placeStack.Children.Add (placeLabel);

			grid.Children.Add (addressStack, 1, 0);
			grid.Children.Add (placeStack, 1, 1);

			var distanceLabel = new Label ();
			distanceLabel.SetBinding (Label.TextProperty, new Binding ("Distance", BindingMode.Default, new DistanceConverter ()));
			distanceLabel.FontSize = 16;
			distanceLabel.VerticalTextAlignment = TextAlignment.Center;

			grid.Children.Add (distanceLabel, 2, 3, 0, 2);
			grid.Padding = 5;

			this.View = grid;
		}

	}

	class DistanceConverter : IValueConverter
	{
		public object Convert (object value, Type targetType, object parameter, CultureInfo culture)
		{
			double val;
			if (double.TryParse (value.ToString (), out val)) {
				return $"{val.ToString ("F")} KM";
			}
			return "";
		}

		public object ConvertBack (object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException ();
		}
	}
}

