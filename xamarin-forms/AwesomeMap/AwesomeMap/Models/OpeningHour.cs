using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace AwesomeMap.Models.Database
{

	public class OpeningHour
	{
		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }

		[ForeignKey (typeof (PetrolStation))]
		public string PetrolStationId { get; set; }

		public int StartHour { get; set; }
		public int EndHour { get; set; }
		public int Weekday { get; set; }
	}
}
