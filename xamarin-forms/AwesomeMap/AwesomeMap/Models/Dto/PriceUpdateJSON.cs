﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace AwesomeMap.Models.Dto
{
	public class FuelPriceDTO
	{
		public DateTime DateOfPrice { get; set; }

		[JsonProperty ("fuelPrice")]
		public double Price { get; set; }

		[JsonProperty ("type")]
		public string Type { get; set; }
	}

	public class PriceUpdateJSON
	{
		[JsonProperty ("petrolStationID")]
		public string PetrolStationID { get; set; }

		[JsonProperty ("fuelPrices")]
		public IList<FuelPriceDTO> FuelPrices { get; set; }
	}

}

