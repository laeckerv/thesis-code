using SQLiteNetExtensions.Attributes;
using SQLite.Net.Attributes;
using System;
using AwesomeMap.Helpers;

namespace AwesomeMap.Models.Database
{
	public class LocationDTO
	{

		//[PrimaryKey, AutoIncrement]
		//public int Id { get; set; }

		//[ForeignKey (typeof (PetrolStation))]
		//public string PetrolStationId { get; set; }

		public double Latitude { get; set; }
		public double Longitude { get; set; }

	}
}
