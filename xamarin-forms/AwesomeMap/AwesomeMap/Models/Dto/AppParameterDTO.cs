namespace AwesomeMap.Models.Dto
{
	public class AppParameterDTO
	{
		public int RefreshPeriodDynamic { get; set; }
		public int RefreshPeriodHolidays { get; set; }
		public int RefreshPeriodPetrolStation { get; set; }
		public int TrackUserPeriod { get; set; }
	}
}
