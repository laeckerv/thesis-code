using System;
using System.Collections.Generic;
using AwesomeMap.Helpers;
using AwesomeMap.Models.Database;

namespace AwesomeMap.Models.Dto
{

	public class PetrolStationDTO : PetrolStation
	{
		public List<OpeningTimeJSON> OpeningTimes { get; set; }
		public LocationDTO Location { get; set; }

		public PetrolStation toPetrolStation ()
		{
			//OpeningHours = new List<OpeningHour> ();
			//if (OpeningTimes != null) {
			//	foreach (var item in OpeningTimes) {
			//		foreach (var day in item.ApplicableDay) {
			//			foreach (var time in item.TimePeriod) {
			//				OpeningHours.Add (new OpeningHour () {
			//					StartHour = (TimeSpan.Parse (time.Start).Hours * 100) + (TimeSpan.Parse (time.Start).Minutes),
			//					EndHour = (TimeSpan.Parse (time.End).Hours * 100) + (TimeSpan.Parse (time.End).Minutes),
			//					Weekday = day.toWeekDay ()
			//				});
			//			}
			//		}
			//	}
			//}

			return new PetrolStation () {
				Id = this.Id,
				Distance = this.Distance,

				Latitude = this.Location.Latitude,
				Longitude = this.Location.Longitude,

				PriceDiesel = this.PriceDiesel,
				PriceE10 = this.PriceE10,
				PriceE5 = this.PriceE5,
				PriceLastUpdate = this.PriceLastUpdate,
				VersionTime = this.VersionTime,

				Brand = this.Brand,
				Name = this.Name,
				Street = this.Street,
				HouseNumber = this.HouseNumber,
				Place = this.Place,
				PostCode = this.PostCode,
				FederalState = this.FederalState,
				Deleted = this.Deleted,
				//OpeningHours = this.OpeningHours
			};
		}
	}

	public class OpeningTimeJSON
	{
		public List<string> ApplicableDay { get; set; }
		public List<TimePeriodJSON> TimePeriod { get; set; }
	}

	public class TimePeriodJSON
	{
		public string Start { get; set; }
		public string End { get; set; }
	}
}
