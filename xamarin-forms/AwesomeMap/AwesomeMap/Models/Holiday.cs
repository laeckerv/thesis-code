﻿using System;

namespace AwesomeMap.Models.Database
{
	public class Holiday
	{
		public DateTime Date { get; set; }
		public string Name { get; set; }
		public string FederalStatesAsString { get; set; }
	}

	
}

