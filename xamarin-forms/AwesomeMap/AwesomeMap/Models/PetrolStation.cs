﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System.Runtime.InteropServices.WindowsRuntime;
using AwesomeMap.Helpers;
using Xamarin.Forms.Maps;

namespace AwesomeMap.Models.Database
{
	public class PetrolStation : IComparable
	{
		[PrimaryKey]
		public string Id { get; set; }

		[Ignore]
		public double Distance { get; set; }
		[Ignore]
		public int ClusterCount { get; set; } = 1;
		[Ignore]
		public bool IsCheapest { get; set; } = false;

		public double Latitude { get; set; }
		public double Longitude { get; set; }

		[Ignore]
		public double TmpLatitude { get; set; }
		[Ignore]
		public double TmpLongitude { get; set; }

		public double PriceDiesel { get; set; }
		public double PriceE10 { get; set; }
		public double PriceE5 { get; set; }
		public DateTime PriceLastUpdate { get; set; }
		public DateTime VersionTime { get; set; }

		public string Brand { get; set; }
		public string Name { get; set; }
		public string Street { get; set; }
		public string HouseNumber { get; set; }
		public string Place { get; set; }
		public string PostCode { get; set; }
		public string FederalState { get; set; }
		public bool Deleted { get; set; }

		//[OneToMany (CascadeOperations = CascadeOperation.All)]
		//public List<OpeningHour> OpeningHours { get; set; }

		[Ignore]
		public Position Position {
			get {
				return new Position (Latitude, Longitude);
			}
			set {
				Latitude = value.Latitude;
				Longitude = value.Longitude;
			}
		}

		[Ignore, JsonIgnore]
		public double Price {
			get {

				switch (Settings.SelectedGasType) {
				case "E5":
					return PriceE5;
				case "E10":
					return PriceE10;
				case "DIESEL":
					return PriceDiesel;
				default:
					return 0;
				}
			}
		}

		public int CompareTo (object obj)
		{
			var o = obj as PetrolStation;
			if (o == null) {
				return 1;

			} else if (o.Equals (this)) {
				return 0;
			} else {
				return (this.Price < o.Price ? -1 : 1);
			}
		}
	}
}

