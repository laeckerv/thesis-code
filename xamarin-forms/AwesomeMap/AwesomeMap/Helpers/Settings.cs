// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;

namespace AwesomeMap.Helpers
{
	/// <summary>
	/// This is the Settings static class that can be used in your Core solution or in any
	/// of your client applications. All settings are laid out the same exact way with getters
	/// and setters. 
	/// </summary>
	public static class Settings
	{
		private static ISettings AppSettings {
			get {
				return CrossSettings.Current;
			}
		}

		#region Setting Constants

		private const string CurrentDbSchemaVersionKey = "DB_SCHEMA_VERSION";
		private static readonly int CurrentDbSchemaVersionDefault = 0;

		private const string ShowTutorialKey = "DB_SCHEMA_VERSION";
		private static readonly bool ShowTutorialDefault = true;

		private const string RefreshPeriodPricesKey = "REFRESH_PERIOD_PRICES";
		private static readonly int RefreshPeriodPricesDefault = 900;

		private const string RefreshPeriodHolidaysKey = "REFRESH_PERIOD_HOLIDAYS";
		private static readonly int RefreshPeriodHolidaysDefault = 2592000;

		private const string RefreshPeriodPetrolStationKey = "REFRESH_PERIOD_PETROLSTATION";
		private static readonly int RefreshPeriodPetrolStationDefault = 86400;

		private const string RefreshPeroiodTrackUser = "REFRESH_PERIOD_TRACK_USER";
		private static readonly int RefreshPeriodTrackUserDefault = 180;

		private const string RefreshPeriodAppParameterKey = "REFRESH_PERIOD_APP_PARAMETER";
		private static readonly int RefreshPeriodAppParameterDefault = 432000;

		private const string LastUpdatePricesKey = "LAST_UPDATE_PRICES";
		private static readonly DateTime LastUpdatePricesDefault = DateTime.MinValue;

		private const string LastUpdateHolidaysKey = "LAST_UPDATE_HOLIDAYS";
		private static readonly DateTime LastUpdateHolidaysDefault = DateTime.MinValue;

		private const string LastUpdatePetrolStationKey = "LAST_UPDATE_PETROLSTATION";
		private static readonly DateTime LastUpdatePetrolStationDefault = DateTime.MinValue;

		private const string LastUpdateTrackUserKey = "LAST_UPDATE_TRACK_USER";
		private static readonly DateTime LastUpdateTrackUserDefault = DateTime.MinValue;

		private const string LastUpdateAppParameterKey = "LAST_UPDATE_APP_PARAMETER";
		private static readonly DateTime LastUpdateAppParameterDefault = DateTime.MinValue;

		private const string PetrolStationVersionTimeKey = "PETROLSTATION_VERSION_TIME";
		private static readonly DateTime PetrolStationVersionTimeDefault = DateTime.MinValue;

		private const string SelectedGasTypeKey = "SELECTED_GAS_TYPE";
		private static readonly string SelectedGasTypeDefault = "E5";

		#endregion

		public static int CurrentDbSchemaVersion {
			get {
				return AppSettings.GetValueOrDefault<int> (CurrentDbSchemaVersionKey, CurrentDbSchemaVersionDefault);
			}
			set {
				AppSettings.AddOrUpdateValue<int> (CurrentDbSchemaVersionKey, value);
			}
		}

		public static bool ShowTutorial {
			get {
				return AppSettings.GetValueOrDefault<bool> (ShowTutorialKey, ShowTutorialDefault);
			}
			set {
				AppSettings.AddOrUpdateValue<bool> (ShowTutorialKey, value);
			}
		}

		#region AppParameter

		public static int RefreshPeriodPrices {
			get {
				return AppSettings.GetValueOrDefault<int> (RefreshPeriodPricesKey, RefreshPeriodPricesDefault);
			}
			set {
				AppSettings.AddOrUpdateValue<int> (RefreshPeriodPricesKey, value);
			}
		}

		public static int RefreshPeriodHolidays {
			get {
				return AppSettings.GetValueOrDefault<int> (RefreshPeriodHolidaysKey, RefreshPeriodHolidaysDefault);
			}
			set {
				AppSettings.AddOrUpdateValue<int> (RefreshPeriodHolidaysKey, value);
			}
		}

		public static int RefreshPeriodPetrolStation {
			get {
				return AppSettings.GetValueOrDefault<int> (RefreshPeriodPetrolStationKey, RefreshPeriodPetrolStationDefault);
			}
			set {
				AppSettings.AddOrUpdateValue<int> (RefreshPeriodPetrolStationKey, value);
			}
		}

		public static int RefreshPeriodTrackUser {
			get {
				return AppSettings.GetValueOrDefault<int> (RefreshPeroiodTrackUser, RefreshPeriodTrackUserDefault);
			}
			set {
				AppSettings.AddOrUpdateValue<int> (RefreshPeroiodTrackUser, value);
			}
		}

		public static int RefreshPeriodAppParameter {
			get {
				return AppSettings.GetValueOrDefault<int> (RefreshPeriodAppParameterKey, RefreshPeriodAppParameterDefault);
			}
			set {
				AppSettings.AddOrUpdateValue<int> (RefreshPeriodAppParameterKey, value);
			}
		}

		public static DateTime LastUpdatePrices {
			get {
				return AppSettings.GetValueOrDefault<DateTime> (LastUpdatePricesKey, LastUpdatePricesDefault);
			}
			set {
				AppSettings.AddOrUpdateValue<DateTime> (LastUpdatePricesKey, value);
			}
		}

		public static DateTime LastUpdateHolidays {
			get {
				return AppSettings.GetValueOrDefault<DateTime> (LastUpdateHolidaysKey, LastUpdateHolidaysDefault);
			}
			set {
				AppSettings.AddOrUpdateValue<DateTime> (LastUpdateHolidaysKey, value);
			}
		}

		public static DateTime LastUpdatePetrolStations {
			get {
				return AppSettings.GetValueOrDefault<DateTime> (LastUpdatePetrolStationKey, LastUpdatePetrolStationDefault);
			}
			set {
				AppSettings.AddOrUpdateValue<DateTime> (LastUpdatePetrolStationKey, value);
			}
		}

		public static DateTime LastUpdateTrackUser {
			get {
				return AppSettings.GetValueOrDefault<DateTime> (LastUpdateTrackUserKey, LastUpdateTrackUserDefault);
			}
			set {
				AppSettings.AddOrUpdateValue<DateTime> (LastUpdateTrackUserKey, value);
			}
		}

		public static DateTime LastUpdateAppParameter {
			get {
				return AppSettings.GetValueOrDefault<DateTime> (LastUpdateAppParameterKey, LastUpdateAppParameterDefault);
			}
			set {
				AppSettings.AddOrUpdateValue<DateTime> (LastUpdateAppParameterKey, value);
			}
		}

		#endregion

		public static DateTime PetrolStationVersionTime {
			get {
				return AppSettings.GetValueOrDefault<DateTime> (PetrolStationVersionTimeKey, PetrolStationVersionTimeDefault);
			}
			set {
				AppSettings.AddOrUpdateValue<DateTime> (PetrolStationVersionTimeKey, value);
			}
		}

		public static string SelectedGasType {
			get {
				return AppSettings.GetValueOrDefault<string> (SelectedGasTypeKey, SelectedGasTypeDefault);
			}
			set {
				AppSettings.AddOrUpdateValue<string> (SelectedGasTypeKey, value);
			}
		}

	}
}