﻿using System;
using AwesomeMap.Models.Database;
using Xamarin.Forms.Maps;
namespace AwesomeMap.Helpers
{
	public static class Haversine
	{
		/// <summary>
		/// Radius of the Earth in Kilometers.
		/// </summary>
		private const double EARTH_RADIUS_KM = 6371;

		/// <summary>
		/// Converts an angle to a radian.
		/// </summary>
		/// <param name="input">The angle that is to be converted.</param>
		/// <returns>The angle in radians.</returns>
		private static double ToRad (double input)
		{
			return input * (Math.PI / 180);
		}

		private static double ToDeg (double input)
		{
			return input * (180 / Math.PI);
		}

		/// Calculates the distance between two geo-points in kilometers using the Haversine algorithm.
		/// </summary>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <returns>A double indicating the distance between the points in KM.</returns>
		public static double GetDistanceKM (Position from, Position to)
		{
			double dLat = ToRad (to.Latitude - from.Latitude);
			double dLon = ToRad (to.Longitude - from.Longitude);

			double a = Math.Pow (Math.Sin (dLat / 2), 2) +
				Math.Cos (ToRad (from.Latitude)) * Math.Cos (ToRad (to.Latitude)) *
				Math.Pow (Math.Sin (dLon / 2), 2);

			double c = 2 * Math.Atan2 (Math.Sqrt (a), Math.Sqrt (1 - a));

			double distance = EARTH_RADIUS_KM * c;
			return distance;
		}


		// <summary>
		/// Calculates the end-point from a given source at a given range (meters) and bearing (degrees).
		/// This methods uses simple geometry equations to calculate the end-point.
		/// </summary>
		/// <param name="source">Point of origin</param>
		/// <param name="range">Range in meters</param>
		/// <param name="bearing">Bearing in degrees</param>
		/// <returns>End-point from the source given the desired range and bearing.</returns>
		public static Position CalculateDerivedPosition (Position position, double range, double bearing)
		{
			double latA = ToRad (position.Latitude);
			double lonA = ToRad (position.Longitude);
			double angularDistance = range / EARTH_RADIUS_KM;
			double trueCourse = ToRad (bearing);

			double lat = Math.Asin (
				Math.Sin (latA) * Math.Cos (angularDistance) +
				Math.Cos (latA) * Math.Sin (angularDistance) * Math.Cos (trueCourse));

			double dlon = Math.Atan2 (
				Math.Sin (trueCourse) * Math.Sin (angularDistance) * Math.Cos (latA),
				Math.Cos (angularDistance) - Math.Sin (latA) * Math.Sin (lat));

			double lon = ((lonA + dlon + Math.PI) % (Math.PI * 2)) - Math.PI;

			return new Position (ToDeg (lat), ToDeg (lon));
		}
	}
}
