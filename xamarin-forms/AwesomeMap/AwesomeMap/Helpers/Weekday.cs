﻿using System;
namespace AwesomeMap.Helpers
{
	public static class WeekdayHelper {
		public enum Weekday { Sunday = 1, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Holiday }

		static public int toWeekDay (this string weekdayShort)
		{
			var weekday = string.Empty;

			switch (weekdayShort.ToUpper ()) {
			case "SUN":
				weekday = "Sunday";
				break;
			case "MON":
				weekday = "Monday";
				break;
			case "TUE":
				weekday = "Tuesday";
				break;
			case "WED":
				weekday = "Wednesday";
				break;
			case "THU":
				weekday = "Thursday";
				break;
			case "FRI":
				weekday = "Friday";
				break;
			case "SAT":
				weekday = "Saturday";
				break;
			case "HOLIDAY":
				weekday = "Holiday";
				break;
			default:
				// Should not occure
				return -1;
			}

			return (int)(Enum.Parse (typeof (Weekday), weekday));
		}
	}
}

