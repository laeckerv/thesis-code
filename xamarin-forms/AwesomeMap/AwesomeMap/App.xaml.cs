﻿using Xamarin.Forms;
using AwesomeMap.Database;
using System.Threading.Tasks;
using System;
using AwesomeMap.Helpers;

namespace AwesomeMap
{
	public partial class App : Application
	{
		private static bool isRunning;

		public App ()
		{
			InitializeComponent ();

			MainPage = new AwesomeMapPage ();
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
			Task.Factory.StartNew (async () => await ServerConnection.Instance.UpdateMetadata ());
			StartWatchDogs ();
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
			isRunning = false;
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
			Task.Factory.StartNew (async () => await ServerConnection.Instance.UpdateMetadata ());
			StartWatchDogs ();

		}

		private void StartWatchDogs ()
		{
			isRunning = true;
			Device.StartTimer (TimeSpan.FromSeconds (Settings.RefreshPeriodAppParameter), () => {
				if (isRunning) {
					Task.Factory.StartNew (async () => {
						await ServerConnection.Instance.UpdateAppParameters ();
					});
				}
				return isRunning;
			});

			Device.StartTimer (TimeSpan.FromSeconds (Settings.RefreshPeriodHolidays), () => {
				if (isRunning) {
					Task.Factory.StartNew (async () => {
						await ServerConnection.Instance.UpdateHolidays ();
					});
				}
				return isRunning;
			});

			Device.StartTimer (TimeSpan.FromSeconds (Settings.RefreshPeriodPetrolStation), () => {
				if (isRunning) {
					Task.Factory.StartNew (async () => {
						await ServerConnection.Instance.UpdatePetrolStations ();
					});
				}
				return isRunning;
			});
		}
	}
}

