﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AwesomeMap.Database;
using AwesomeMap.Helpers;
using AwesomeMap.Models.Database;
using Xamarin.Forms.Maps;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Plugin.Geolocator;

namespace AwesomeMap.Utils
{
	public class PsMap : Map
	{
		private IEnumerable<PetrolStation> _allPetrolStations { get; set; }
		public ObservableCollection<PetrolStation> VisiblePetrolStations { get; set; }
		public MapSpan InitialRegion { get; set; }
		public event EventHandler VisbiblePetrolStationsChanged;
		public Position UserPosition { get; set; }

		public PsMap() : base()
		{
			// TODO: Init initial region and inital location.
			Init();
		}

		public PsMap(MapSpan region) : base(region)
		{
			// TODO: Init inital location.
			InitialRegion = region;
			Init();
		}

		/// <summary>
		/// Load the petrol station from the database. If region of the map is known, the petrol stations closer to the
		/// location will be loaded first.
		/// </summary>
		private void Init()
		{
			VisiblePetrolStations = new ObservableCollection<PetrolStation>();
			var locator = CrossGeolocator.Current;

			locator.PositionChanged += (sender, e) => {
				UserPosition = new Position(e.Position.Latitude, e.Position.Longitude);
			};
			locator.StartListeningAsync(100, 10, false);

			this.PropertyChanged += (sender, e) => {
				if (e.PropertyName == "VisibleRegion" && VisibleRegion != null)
					VisibleRegionChanged(VisibleRegion);
			};


			Task.Factory.StartNew(async () => {

				//var tmpPosition = await locator.GetPositionAsync (10000);
				//UserPosition = new Position (tmpPosition.Latitude, tmpPosition.Longitude);

				if (VisibleRegion != null) {
					InitialRegion = new MapSpan(UserPosition, VisibleRegion.LatitudeDegrees, VisibleRegion.LongitudeDegrees);
				}

				if (InitialRegion != null) {
					var northEast = Haversine.CalculateDerivedPosition(InitialRegion.Center, 50, 45);
					var southWest = Haversine.CalculateDerivedPosition(InitialRegion.Center, 50, 225);
					using (var db = new Database.Database()) {
						_allPetrolStations = await db.GetPetrolStationsInRegion(northEast.Latitude, northEast.Longitude, southWest.Latitude, southWest.Longitude);
					}
					UpdateMarkersToVisibleRegion(InitialRegion);
				}
			}).ContinueWith(async (res) => {
				using (var db = new Database.Database()) {
					_allPetrolStations = await db.GetPetrolStations();
				}
				UpdateMarkersToVisibleRegion(InitialRegion);
			});

		}

		public void UpdateMarkersToVisibleRegion(MapSpan visibleRegion)
		{
			var cts = new CancellationTokenSource().Token;

			Task.Factory.StartNew(async () => {
				if (visibleRegion.Radius.Kilometers <= 20) {

					var locator = CrossGeolocator.Current;
					var tmpPosition = await locator.GetPositionAsync(10000);
					UserPosition = new Position(tmpPosition.Latitude, tmpPosition.Longitude);

					var tmp = _allPetrolStations.Where(p => Haversine.GetDistanceKM(visibleRegion.Center, p.Position) < visibleRegion.Radius.Kilometers);
					if (cts.IsCancellationRequested)
						return;
					await ServerConnection.Instance.UpdatePriceForPetrolStations(tmp);
					if (cts.IsCancellationRequested)
						return;
					tmp = tmp.Where(p => p.Price > 0).OrderBy(p => p.Price);

					if (cts.IsCancellationRequested)
						return;

					Device.BeginInvokeOnMainThread(() => {
						PetrolStation cheapestStation = null;

						VisiblePetrolStations.Clear();
						foreach (var item in tmp) {
							item.ClusterCount = 1;
							item.IsCheapest = false;
							item.Distance = Haversine.GetDistanceKM(UserPosition, item.Position);

							if (cheapestStation == null) {
								cheapestStation = item;
								item.IsCheapest = true;
							}

							if (cheapestStation.Price > item.Price) {
								cheapestStation.IsCheapest = false;
								cheapestStation = item;
								item.IsCheapest = true;
							} else if (cheapestStation.Price == item.Price) {
								// Find the closer station.
								if (item.Distance < cheapestStation.Distance) {
									cheapestStation.IsCheapest = false;
									cheapestStation = item;
									item.IsCheapest = true;
								}
							}
							VisiblePetrolStations.Add(item);
						}
						VisbiblePetrolStationsChanged?.Invoke(this, null);
					});
				} else {
					Device.BeginInvokeOnMainThread(() => {
						VisiblePetrolStations.Clear();
					});
					VisbiblePetrolStationsChanged?.Invoke(this, null);
				}

			});
		}

		private void cluster(IEnumerable<PetrolStation> tmpList, MapSpan visibleRegion)
		{
			//foreach (var item in tmpList) {
			//	var inRangeList = VisiblePetrolStations.Where (ps => Haversine.GetDistanceKM (ps.Position, item.Position) < 5).OrderBy (p => p.Price);
			//	if (inRangeList.Count () > 0) {
			//		if (inRangeList.First ().CompareTo (item) > 0) {
			//			foreach (var it in inRangeList) {
			//				item.ClusterCount += it.ClusterCount;
			//				VisiblePetrolStations.Remove (it);
			//			}
			//			VisiblePetrolStations.Add (item);
			//		} else {
			//			VisiblePetrolStations.First (x => x.Id == inRangeList.First ().Id).ClusterCount++;
			//		}
			//	} else {
			//		VisiblePetrolStations.Add (item);
			//	}
			//}
		}

		public void VisibleRegionChanged(MapSpan visibleRegion)
		{
			UpdateMarkersToVisibleRegion(visibleRegion);
		}
	}
}

