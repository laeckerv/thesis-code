﻿using System;
namespace AwesomeMap.Utils
{
	public interface ILogger
	{
		void Debug (string msg);

		void Info (string msg);

		void Warn (string msg);

		void Error (string msg, Exception e = null);

		void FatalError (string msg, Exception e = null);

	}
}

