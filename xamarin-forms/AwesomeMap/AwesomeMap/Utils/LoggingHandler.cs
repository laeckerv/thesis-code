﻿using System;
using Xamarin.Forms;

namespace AwesomeMap.Utils
{
	public class LoggingHandler
	{
		public enum LogLevels { debug, info, warn, error, fatal, no_logging };

		public LogLevels LogLevel { get; set; }

		private static volatile LoggingHandler instance;
		private static object syncRoot = new Object ();
		public static LoggingHandler Instance {
			get {
				if (instance == null) {
					lock (syncRoot) {
						if (instance == null)
							instance = new LoggingHandler ();
					}
				}

				return instance;
			}
		}

		private ILogger logger = null;

		private LoggingHandler ()
		{
			// Set default log level.
			LogLevel = LogLevels.warn;

			// Ioc
			logger = DependencyService.Get<ILogger> ();
		}

		public void Debug (string msg)
		{
			if (logger != null && LogLevel <= LogLevels.debug) {
				logger.Debug (msg);
			}
		}

		public void Info (string msg)
		{
			if (logger != null && LogLevel <= LogLevels.info) {
				logger.Info (msg);
			}
		}

		public void Warn (string msg)
		{
			if (logger != null && LogLevel <= LogLevels.warn) {
				logger.Warn (msg);
			}
		}

		public void Error (string msg, Exception e = null)
		{
			if (logger != null && LogLevel <= LogLevels.error) {
				logger.Error (msg, e);
			}
		}

		public void FatalError (string msg, Exception e = null)
		{
			if (logger != null && LogLevel <= LogLevels.error) {
				logger.FatalError (msg, e);
			}
		}
	}
}

