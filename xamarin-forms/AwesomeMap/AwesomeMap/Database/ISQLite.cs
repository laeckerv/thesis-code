﻿using SQLite.Net.Async;
using System.IO;

namespace AwesomeMap.Database
{
	public interface ISQLite
	{
		SQLiteAsyncConnection GetConnection (string fileName);
		bool DbExists (string fileName);
		void DbCopy (string fileName, byte [] database);
	}
}


