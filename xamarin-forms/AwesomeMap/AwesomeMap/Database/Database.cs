﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AwesomeMap.Helpers;
using AwesomeMap.Models.Database;
using AwesomeMap.Utils;
using SQLite.Net.Async;
using SQLiteNetExtensionsAsync.Extensions;
using System.Threading;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace AwesomeMap.Database
{
	public class Database : IDisposable
	{
		private readonly SQLiteAsyncConnection database = null;
		private static readonly AsyncLock locker = new AsyncLock ();

		private DbConnectionManager dbConnectionManager;

		private Database (DbConnectionManager dbConnectionManager)
		{
			this.dbConnectionManager = dbConnectionManager;
		}

		public Database () : this (DbConnectionManager.Instance)
		{
			database = dbConnectionManager.GetConnection ();
		}

		public void Dispose ()
		{
			if (database != null) {
				DbConnectionManager.Instance.FreeConnection (database);
			}
		}

		public async Task SaveHolidays (IList<Holiday> holidayList)
		{
			if (database != null) {
				LoggingHandler.Instance.Debug ("Delete all holidays from database.");
				await database.DeleteAllAsync<Holiday> ();
				using (await locker.LockAsync ()) {
					await database.InsertAllAsync (holidayList);
				}
			}
		}

		public async Task<IList<Holiday>> GetHolidays ()
		{
			if (database != null) {
				return await database.GetAllWithChildrenAsync<Holiday> ();
			}
			return null;
		}

		public async Task SavePetrolStations (IList<PetrolStation> petrolStations)
		{
			if (database != null) {
				using (await locker.LockAsync ()) {
					await database.InsertOrReplaceAllWithChildrenAsync (petrolStations);
				}
			}
		}

		public async Task SavePetrolStation (PetrolStation petrolStation)
		{
			if (database != null) {
				using (await locker.LockAsync ()) {
					await database.InsertOrReplaceWithChildrenAsync (petrolStation);
				}
			}
		}

		/// <summary>
		/// Gets all patrol stations (also invalid ones).
		/// </summary>
		/// <returns>The all petrol stations.</returns>
		/// <param name="includeDeleted">Include deleted.</param>
		public async Task<IList<PetrolStation>> GetAllPetrolStations (bool includeDeleted = false)
		{
			if (database != null) {
				return await database.GetAllWithChildrenAsync<PetrolStation> (p => (p.Deleted == false || p.Deleted == includeDeleted));
			}
			return null;
		}

		/// <summary>
		/// Gets all  petrol stations except the invalid ones.
		/// </summary>
		/// <returns>The petrol stations.</returns>
		/// <param name="includeDeleted">Include deleted.</param>
		public async Task<IList<PetrolStation>> GetPetrolStations (bool includeDeleted = false)
		{
			if (database != null) {
				return await database.GetAllWithChildrenAsync<PetrolStation> (p => (p.Deleted == false || p.Deleted == includeDeleted) && p.Brand != string.Empty);
			}
			return null;
		}

		public async Task<IList<PetrolStation>> GetPetrolStationsInRegion (double north, double east, double south, double west, bool includeDeleted = false)
		{
			if (database != null) {
				//return await database.GetAllWithChildrenAsync<PetrolStation> (p => (p.Deleted == false || p.Deleted == includeDeleted) && p.IsWithinBounds (north, east, south, west));
				return await database.GetAllWithChildrenAsync<PetrolStation> (p => (p.Deleted == false || p.Deleted == includeDeleted) && (
					p.Latitude <= north &&
					p.Latitude >= south &&
					p.Longitude <= east &&
					p.Longitude >= west
				));
			}
			return null;
		}

		public async Task ClearPrices (IList<PetrolStation> petrolStations)
		{
			if (database != null) {
				foreach (var item in petrolStations) {
					item.PriceE5 = 0;
					item.PriceE10 = 0;
					item.PriceDiesel = 0;
				}
				using (await locker.LockAsync ()) {
					await database.InsertOrReplaceAllWithChildrenAsync (petrolStations);
				}
			}
		}

		public async Task<DateTime> GetLatestVersionTime ()
		{
			if (database != null) {
				var result = await database.QueryAsync<PetrolStation> ("SELECT * FROM PetrolStation ORDER BY VersionTime DESC LIMIT 1");
				if (result.Count > 0) {
					return result.First ().VersionTime;
				}
			}
			return Settings.PetrolStationVersionTime;
		}
	}
}

