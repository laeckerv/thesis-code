﻿using System;
using System.IO;
using Android.Content.Res;
using AwesomeMap.Database;
using SQLite.Net;
using SQLite.Net.Async;
using SQLite.Net.Platform.XamarinAndroid;
using Xamarin.Forms;


[assembly: Dependency (typeof (SQLiteAndroid))]
public class SQLiteAndroid : ISQLite
{
	public void DbCopy (string fileName, byte [] database)
	{
		var path = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), fileName);
		File.WriteAllBytes (path, database);
	}

	public bool DbExists (string fileName)
	{
		var path = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), fileName);
		return File.Exists (path);
	}

	public SQLiteAsyncConnection GetConnection (string fileName)
	{
		var path = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), fileName);

		var platform = new SQLitePlatformAndroid ();
		var connectionWithLock = new SQLiteConnectionWithLock (
									 platform,
									 new SQLiteConnectionString (path, true)
		);

		return new SQLiteAsyncConnection (() => connectionWithLock);
	}
}

