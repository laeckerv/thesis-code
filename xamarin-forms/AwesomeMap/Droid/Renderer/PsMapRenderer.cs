﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Graphics;
using Android.Locations;
using Android.Views;
using Android.Widget;
using AwesomeMap.Database;
using AwesomeMap.Droid.Renderer;
using AwesomeMap.Helpers;
using AwesomeMap.Models.Database;
using AwesomeMap.Utils;
using Xamarin.Forms;
using Xamarin.Forms.Maps.Android;
using Android.Media.Midi;
using System.Dynamic;
using Android.Util;
using Org.Apache.Http.Conn.Params;
using Xamarin.Forms.Maps;

[assembly: ExportRendererAttribute (typeof (PsMap), typeof (PsMapRenderer))]

namespace AwesomeMap.Droid.Renderer
{
	public class PsMapRenderer : MapRenderer, IOnMapReadyCallback, GoogleMap.IOnMarkerClickListener, GoogleMap.IOnMapClickListener, GoogleMap.IOnMapLongClickListener
	{

		private GoogleMap map;
		private PsMap psMap;
		private int onMapMarkerWidth;
		private CameraPosition curCameraPosition;
		private Marker onMapMarker;
		private Polyline onMapPolyline;

		Android.Views.View mapMarker;
		TextView mapMarkerBrandText;
		TextView mapMarkerPriceText;
		TextView mapMarkerPriceSmallText;
		TextView mapMarkerClusterText;

		public void OnMapReady (GoogleMap googleMap)
		{
			map = googleMap;

			map.UiSettings.ZoomControlsEnabled = false;
			map.UiSettings.MyLocationButtonEnabled = false;
			map.SetOnMapClickListener (this);
			map.SetOnMapLongClickListener (this);
			map.SetOnMarkerClickListener (this);
		}

		protected override void OnElementChanged (Xamarin.Forms.Platform.Android.ElementChangedEventArgs<Xamarin.Forms.View> e)
		{
			base.OnElementChanged (e);

			if (e.OldElement != null) {
				psMap.VisbiblePetrolStationsChanged -= updateMarkers;
				psMap = null;
				map.SetOnMapClickListener (null);
				map.SetOnMapLongClickListener (null);
				map.SetOnMarkerClickListener (null);
			}

			if (e.NewElement != null) {
				psMap = e.NewElement as PsMap;
				psMap.VisbiblePetrolStationsChanged += updateMarkers;

				mapMarker = Inflate (Context, Resource.Layout.map_marker, null);
				mapMarkerBrandText = mapMarker.FindViewById<TextView> (Resource.Id.map_marker_name);
				mapMarkerPriceText = mapMarker.FindViewById<TextView> (Resource.Id.map_marker_price);
				mapMarkerPriceSmallText = mapMarker.FindViewById<TextView> (Resource.Id.map_marker_price_smallNumber);
				mapMarkerClusterText = mapMarker.FindViewById<TextView> (Resource.Id.map_marker_count);

				mapMarker.Measure (MeasureSpec.MakeMeasureSpec (0, MeasureSpecMode.Unspecified),
								   MeasureSpec.MakeMeasureSpec (0, MeasureSpecMode.Unspecified));

				onMapMarkerWidth = mapMarker.MeasuredWidth;

				((MapView)Control).GetMapAsync (this);
			}
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);
		}


		private void updateMarkers (object sender, EventArgs e)
		{
			if (map != null) {
				//var stations = cluster (radius, cts.Token);
				var stations = psMap.VisiblePetrolStations;
				var radius = psMap.VisibleRegion?.Radius.Kilometers;
				DisplayMetrics dM = new DisplayMetrics ();
				Display.GetMetrics (dM);
				var dWidth = psMap.Width * dM.ScaledDensity / onMapMarkerWidth;

				LoggingHandler.Instance.Debug ($"Amount of stations to render: {stations.Count ()}");


				Device.BeginInvokeOnMainThread (() => {
					map.Clear ();
				});


				// Draw markers on map.
				var newOnMap = new Dictionary<string, Tuple<PetrolStation, Marker>> ();

				foreach (var station in stations) {
					var onMapAlready = newOnMap.Where (pm => Haversine.GetDistanceKM (pm.Value.Item1.Position, station.Position) < 2 * radius / dWidth).OrderBy (pm => pm.Value.Item1.Price);
					if (onMapAlready.Count () > 0) {
						if (onMapAlready.First ().Value.Item1.CompareTo (station) > 0) {
							Device.BeginInvokeOnMainThread (() => {
								foreach (var it in onMapAlready) {
									var tmpStation = it.Value.Item1;
									var tmpMarker = it.Value.Item2;

									station.ClusterCount += tmpStation.ClusterCount;
									tmpMarker.Remove ();
									newOnMap.Remove (tmpStation.Id);
								}

								// Add new marker to map.
								var markerOption = new MarkerOptions ();
								markerOption.SetPosition (new LatLng (station.Latitude, station.Longitude));
								markerOption.SetIcon (getPsPinBitmapDescriptor (station));
								newOnMap.Add (station.Id, Tuple.Create (station, map.AddMarker (markerOption)));
							});
						} else {

							Device.BeginInvokeOnMainThread (() => {
								var tmpStation = onMapAlready.First ().Value.Item1;
								var tmpMarker = onMapAlready.First ().Value.Item2;
								tmpMarker.Remove ();
								newOnMap.Remove (tmpStation.Id);

								tmpStation.ClusterCount++;

								// Add new marker to map.
								var markerOption = new MarkerOptions ();
								markerOption.SetPosition (new LatLng (tmpStation.Latitude, tmpStation.Longitude));
								markerOption.SetIcon (getPsPinBitmapDescriptor (tmpStation));
								newOnMap.Add (tmpStation.Id, Tuple.Create (tmpStation, map.AddMarker (markerOption)));
							});
						}
					} else {

						// Add new marker to map.
						Device.BeginInvokeOnMainThread (() => {
							var markerOption = new MarkerOptions ();
							markerOption.SetPosition (new LatLng (station.Latitude, station.Longitude));
							markerOption.SetIcon (getPsPinBitmapDescriptor (station));
							newOnMap.Add (station.Id, Tuple.Create (station, map.AddMarker (markerOption)));
						});
					}
				}

				//// Delete old markers from the map.
				//// TODO: Check if hiding them would be more performant then acutally delting them.
				//foreach (var key in onMapMarkers.Keys.Where (k => !newOnMap.ContainsKey (k))) {
				//	Marker marker;
				//	if (onMapMarkers.TryGetValue (key, out marker)) {
				//		Device.BeginInvokeOnMainThread (() => marker.Remove ());
				//	}
				//}

				//onMapMarkers = newOnMap;
			}
		}

		private void setValuesOnMarker (int clusterCount, bool isCheapest, double price, string brand)
		{

			if (clusterCount > 1) {
				if (clusterCount > 99) {
					clusterCount = 99;
				}
				if (isCheapest)
					mapMarker.SetBackgroundResource (Resource.Drawable.annotation_clustering_cheapest);
				else
					mapMarker.SetBackgroundResource (Resource.Drawable.annotation_clustering);
				mapMarkerClusterText.SetText (clusterCount.ToString (), TextView.BufferType.Normal);

				var pT = mapMarkerClusterText.PaddingTop;
				var pR = mapMarkerClusterText.PaddingRight;
				var pB = mapMarkerClusterText.PaddingBottom;
				var pL = mapMarkerClusterText.PaddingLeft;

				if (clusterCount > 9) {
					mapMarkerClusterText.SetPadding (0, pT, -1, pB);
				} else {
					mapMarkerClusterText.SetPadding (0, pT, 4, pB);
				}
			} else {
				if (isCheapest)
					mapMarker.SetBackgroundResource (Resource.Drawable.annotation_cheapest);
				else
					mapMarker.SetBackgroundResource (Resource.Drawable.annotation);
				mapMarkerClusterText.SetText (string.Empty, TextView.BufferType.Normal);
			}

			mapMarkerBrandText.SetText (brand, TextView.BufferType.Normal);

			var priceAsString = price.ToString ();
			if (priceAsString.Length == 4) {
				mapMarkerPriceText.SetText (priceAsString, TextView.BufferType.Normal);
				mapMarkerPriceSmallText.SetText ("0", TextView.BufferType.Normal);
			} else if (priceAsString.Length == 5) {
				mapMarkerPriceText.SetText (priceAsString.Remove (4, 1), TextView.BufferType.Normal);
				mapMarkerPriceSmallText.SetText (priceAsString.Remove (0, 4), TextView.BufferType.Normal);
			} else {
				mapMarkerPriceText.SetText ("n/a", TextView.BufferType.Normal);
				mapMarkerPriceSmallText.SetText (string.Empty, TextView.BufferType.Normal);
			}
		}

		private BitmapDescriptor getPsPinBitmapDescriptor (PetrolStation station)
		{
			setValuesOnMarker (station.ClusterCount, station.IsCheapest, station.Price, station.Brand);

			mapMarker.Measure (MeasureSpec.MakeMeasureSpec (0, MeasureSpecMode.Unspecified),
							   MeasureSpec.MakeMeasureSpec (0, MeasureSpecMode.Unspecified));

			mapMarker.Layout (0, 0, mapMarker.MeasuredWidth, mapMarker.MeasuredHeight);

			using (var mapMarkerBitmap = Bitmap.CreateBitmap (mapMarker.MeasuredWidth, mapMarker.MeasuredHeight, Bitmap.Config.Argb8888)) {
				using (var canvas = new Canvas (mapMarkerBitmap)) {
					mapMarker.Draw (canvas);
				}
				return BitmapDescriptorFactory.FromBitmap (mapMarkerBitmap);
			}
		}

		public bool OnMarkerClick (Marker marker)
		{
			onMapMarker?.Remove ();
			onMapPolyline?.Remove ();

			var markerPosition = marker.Position;
			Task.Factory.StartNew (async () => {
				IEnumerable<Tuple<double, double>> route = await ServerConnection.Instance.CalculateRoute (psMap.UserPosition.Latitude, psMap.UserPosition.Longitude, markerPosition.Latitude, markerPosition.Longitude);

				var polyLineOptions = new PolylineOptions ();
				polyLineOptions.Geodesic (true);
				polyLineOptions.InvokeWidth (8);
				polyLineOptions.InvokeColor (Android.Graphics.Color.Argb (255, 0, 0, 255));
				foreach (var coord in route) {

					polyLineOptions.Add (new LatLng (coord.Item1, coord.Item2));
				}

				Device.BeginInvokeOnMainThread (() => {
					onMapPolyline = map.AddPolyline (polyLineOptions);
					animateCameraToShow (markerPosition, new LatLng (psMap.UserPosition.Latitude, psMap.UserPosition.Longitude), onMapMarkerWidth);
				});

			});
			return true;
		}

		public void OnMapClick (LatLng point)
		{
			if (curCameraPosition != null) {
				map.AnimateCamera (CameraUpdateFactory.NewCameraPosition (curCameraPosition));
				curCameraPosition = null;
			}

			onMapMarker?.Remove ();
			onMapPolyline?.Remove ();
		}

		public void OnMapLongClick (LatLng point)
		{
			onMapMarker?.Remove ();
			onMapPolyline?.Remove ();
			onMapMarker = map.AddMarker (new MarkerOptions ().SetPosition (point));

			Task.Factory.StartNew (async () => {
				IEnumerable<Tuple<double, double>> route = await ServerConnection.Instance.CalculateRoute (psMap.UserPosition.Latitude, psMap.UserPosition.Longitude, point.Latitude, point.Longitude);
				var polyLineOptions = new PolylineOptions ();
				foreach (var coord in route) {
					polyLineOptions.Add (new LatLng (coord.Item1, coord.Item2));
				}
				Device.BeginInvokeOnMainThread (() => {
					onMapPolyline = map.AddPolyline (polyLineOptions);
					animateCameraToShow (point, new LatLng (psMap.UserPosition.Latitude, psMap.UserPosition.Longitude), onMapMarkerWidth);
				});
			});
		}

		private void animateCameraToShow (LatLng source, LatLng target, int padding)
		{
			double north, east, south, west;

			if (curCameraPosition == null) {
				curCameraPosition = map.CameraPosition;
			}

			if (source.Latitude > target.Latitude) {
				north = source.Latitude;
				south = target.Latitude;
			} else {
				north = target.Latitude;
				south = source.Latitude;
			}

			if (source.Longitude > target.Longitude) {
				east = source.Longitude;
				west = target.Longitude;
			} else {
				east = target.Longitude;
				west = source.Longitude;
			}

			map.AnimateCamera (CameraUpdateFactory.NewLatLngBounds (
				new LatLngBounds (new LatLng (south, west), new LatLng (north, east)), padding));
		}
	}
}

