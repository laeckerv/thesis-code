﻿using System;
using AwesomeMap.Utils;
using Xamarin.Forms;

[assembly: Dependency (typeof (Logger))]
class Logger : ILogger
{
	public void Debug (string msg)
	{
		Console.WriteLine ($"[DEBUG {DateTime.Now.ToString ("hh:mm:ss")}]: {msg}");
	}

	public void Info (string msg)
	{
		Console.WriteLine ($"[INFO {DateTime.Now.ToString ("hh:mm:ss")}]: {msg}");
	}

	public void Warn (string msg)
	{
		Console.WriteLine ($"[WARN {DateTime.Now.ToString ("hh:mm:ss")}]: {msg}");
	}

	public void Error (String msg, Exception e = null)
	{
		if (e == null) {
			Console.WriteLine ($"[ERROR {DateTime.Now.ToString ("hh:mm:ss")}]: {msg}");
		} else {
			Console.WriteLine ($"[ERROR {DateTime.Now.ToString ("hh:mm:ss")}]: {msg} | ERROR: {e.Message}");
			Console.WriteLine (e);
		}
	}

	public void FatalError (String msg, Exception e = null)
	{
#if DEBUG
		if (e == null) {
			Console.WriteLine ($"[FATAL {DateTime.Now.ToString ("hh:mm:ss")}]: {msg}");
		} else {
			Console.WriteLine ($"[FATAL {DateTime.Now.ToString ("hh:mm:ss")}]: {msg} | ERROR: {e.Message}");
			Console.WriteLine (e);
		}
#elif HOCKEY_CRASH
			HockeyApp.TraceWriter.WriteTrace (e, false);
#endif
	}
}

