var React = require('react');
var ReactNative = require('react-native');
var {
  StyleSheet,
  View,
  Text,
} = ReactNative;

var PetrolStationMarker = React.createClass({
  getDefaultProps() {
    return {
      fontSize: 13,
    };
  },
  render() {
    return (
      <View style={styles.container}>
        <View style={[styles.bubble, { borderColor: (this.props.selected ? 'red' : 'rgb(225,222,217)' )}]}>
          <Text numberOfLines={1} style={styles.brand}>{this.props.info.brand}</Text>
          <View style={[{ flexDirection: 'row'}, { margin: 0 },]} >
            <Text style={styles.amount}>{this.props.amount.toString().substring(0,4)}</Text>
            <Text style={styles.amountUpper}>{this.props.amount.toString().substring(4,5)}</Text>
          </View>
        </View>
          <View style={[styles.clusterContainer, { opacity: Number(this.props.info.cluster > 0) }, { borderColor: (this.props.selected ? 'red' : 'rgb(225,222,217)')}]} >
            <Text style={styles.cluster}>{this.props.info.cluster}</Text>
          </View>
        <View style={[styles.arrowBorder, { borderTopColor: (this.props.selected ? 'red' : 'rgb(225,222,217)')}]} />
        <View style={styles.arrow} />
      </View>
    );
  },
});

var styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignSelf: 'flex-start',
  },
  bubble: {
    flexDirection: 'column',
    alignSelf: 'flex-start',
    backgroundColor: '#FFF',
    width: 70,
    padding: 2,
    margin:20,
    borderRadius: 3,
    borderWidth: 1,
  },
  clusterContainer: {
    width:20,
    height:20,
    top: -78,
    left: 60,
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 10,
  }, 
  cluster: {
    backgroundColor: 'transparent',
    textAlign: 'center',
  },
  brand: {
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 0,
    color: 'red',
    flexDirection: 'row',
    flex: 1,
    fontSize: 12,
  },
  amount: {
    margin:5,
    marginTop:0,
    marginRight:0,
    color: '#000',
    fontSize: 24,
  },
  amountUpper: {
    marginRight:5,
    marginBottom:5,
    marginTop:5,
    marginLeft:0,
    color: '#000',
    fontSize: 12,
  },
  arrow: {
    backgroundColor: 'transparent',
    borderWidth: 7,
    borderColor: 'transparent',
    borderTopColor: '#FFF',
    alignSelf: 'center',
    marginTop: -16,
  },
  arrowBorder: {
    backgroundColor: 'transparent',
    borderWidth: 8,
    borderColor: 'transparent',
    alignSelf: 'center',
    marginTop: -41,
  },
});

module.exports = PetrolStationMarker;