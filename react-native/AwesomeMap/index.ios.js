var React = require('react');
var ReactNative = require('react-native');
var {
  AppRegistry,
  MapView,
  View,
} = ReactNative;

var App = require('./App');

var AwesomeMap = React.createClass({
  render() {
    return (
      <App />
    )
  },
});

AppRegistry.registerComponent('AwesomeMap', () => AwesomeMap);