/* @flow */

var React = require('react')
var ReactNative = require('react-native')
var { PropTypes } = React
var {
  Animated,
  Dimensions,
  Image,
  ListView,
  PanResponder,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  View,
  } = ReactNative

var MapView = require('react-native-maps')
var SQLite = require('react-native-sqlite-storage')
var TimerMixin = require('react-timer-mixin')
var PushNotification = require('react-native-push-notification');

var Buffer = require('buffer/').Buffer
var PetrolStationMarker = require('./PetrolStationMarker')
var Moment = require('moment')

let { width, height } = Dimensions.get('window')

const ASPECT_RATIO = width / height
const LATITUDE = 47.672175
const LONGITUDE = 9.154588
const LATITUDE_DELTA = 0.015
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO
const SPACE = 0.01
let id = 0
let db = SQLite.openDatabase({name: "database", createFromLocation: "~www/database.sqlite"}, openCB, errorCB)

SQLite.enablePromise(true);

PushNotification.configure({

    // (optional) Called when Token is generated (iOS and Android)
    onRegister: function(token) {
        console.log( 'TOKEN:', token );
    },

    // (required) Called when a remote or local notification is opened or received
    onNotification: function(notification) {
        console.log(`NOTIFICATION: ${notification}`)
        PushNotification.localNotification(notification)
    },

    // ANDROID ONLY: (optional) GCM Sender ID.
    senderID: "1048377029700",

    // IOS ONLY (optional): default: all - Permissions to register.
    permissions: {
        alert: true,
        badge: true,
        sound: true
    },

    // Should the initial notification be popped automatically
    // default: true
    popInitialNotification: true,

    /**
      * IOS ONLY: (optional) default: true
      * - Specified if permissions will requested or not,
      * - if not, you must call PushNotificationsHandler.requestPermissions() later
      */
    requestPermissions: true,
});

var errorCB = function(err) {
  console.log("SQL Error: " + err)
}
var successCB = function() {
  console.log("SQL executed fine")
}
var openCB = function() {
  console.log("Database OPENED")
}

const App = React.createClass({
  watchID: -1,
  mixins: [TimerMixin],

  componentWillMount: function() {
    this._panResponder = PanResponder.create({
      onMoveShouldSetResponderCapture: () => true,
      onMoveShouldSetPanResponderCapture: () => true,

      onPanResponderGrant: (e, gestureState) => {
        this.state.pan.setOffset({x: 0, y: this.state.pan.y._value});
        this.state.pan.setValue({x: 0, y: height-55});
      },

      onPanResponderMove: Animated.event([
       null, {dx: new Animated.Value(0), dy: this.state.pan.y},
      ]),

      onPanResponderRelease: (e, {vx, vy}) => {
        this.state.pan.flattenOffset()
        if(this.state.pan.y._value < -100)
          Animated.spring(this.state.pan.y, {toValue: -120}).start()
        else 
          Animated.spring(this.state.pan.y, {toValue: 0}).start()
      },
    })
  },
  
  componentDidMount: function() {
    this.setTimeout(
      () => { this.updatePetrolCosts() },
      2500)
    this.setInterval(
      () => { this.updatePetrolCosts() },
      300000
    )
    navigator.geolocation.getCurrentPosition(
      (position) => {
        let initialPosition = position
        this.setState({initialPosition})
      },
      (error) => console.log(error.message),
      {enableHighAccuracy: false, timeout: 20000, maximumAge: 1000}
    )

    this.watchID = navigator.geolocation.watchPosition((position) => {
      let lastPosition = position
      this.setState({lastPosition})
      },
      (error) => console.log(error.message),
    )
  },

  componentWillUnmount: function() {
    navigator.geolocation.clearWatch(this.watchID)
  },

  getInitialState: function() {
    let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    return {
      dataSource: ds,
      pan: new Animated.ValueXY(),
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      lastPosition: undefined,
      initialPosition: undefined,
      versiontime: 0,
      followUserLocation: true,
      polyline: [],
      userSelectedMarker: false,
      selectedMarkerCoordinate: {
        latitude: undefined,
        longitude: undefined,
      },
      selectedMarkerCost: undefined,
      selectedGasType: 'E5',
      markers: [],
      distance: undefined,
      traveltime: undefined,
    }
  },

  onRegionChangeComplete: function(region) {
    this.setState({ region })
    this.loadPetrolStations()
    
    this.log()
  },

  markerIsSelected: function(marker) {
    return (marker.coordinate.latitude === this.state.selectedMarkerCoordinate.latitude &&
            marker.coordinate.longitude === this.state.selectedMarkerCoordinate.longitude)
  },

  updatePetrolStation: function() {
    let query = `SELECT ZVERSIONTIME FROM ZPETROLSTATION ORDER BY ZVERSIONTIME ASC LIMIT 1`
    db.transaction((tx) => {
      tx.executeSql(query, [], (tx, results) => {
          if(results.rows.length > 0) {
            console.log
            let versiontime = Moment(new Date (new Date('01.01.2001').getTime() + results.rows.item(0).ZVERSIONTIME * 1000)).format("YYYY-MM-DD[T]HH:mm:ssZZ").replace('+', '%2b')
            this.setState({
              versiontime: versiontime
            })
            let url = `https://api.spritradarapp.de/v1/petrolstations/changes?versiontime=${versiontime}`
            console.log(url)
            let username = 'spr1.0.0'
            let password = 'zdBAF7jfvDwR2HrBYFjoxtEpcYfAdyhYi'
            const hash = new Buffer(`${username}:${password}`).toString('base64')
            fetch(url, {method: "GET", headers: {'Authorization': `Basic ${hash}`}})
            .then((response) => response.json())
            .then((responseData) => {
                responseData.map(entry => {
                    let queryTwo = ""
                    if(entry.deleted) {
                      queryTwo = `DELETE FROM ZPETROLSTATION WHERE ZSERVERID = '${entry.id}'`
                    }
                    else {
                      queryTwo = `INSERT OR REPLACE INTO ZPETROLSTATION (ZSERVERID, ZBRAND, ZLOCATIONLATITUDE, ZLOCATIONLONGITUDE, ZVERSIONTIME) VALUES ('${entry.id}', '${entry.brand}', ${entry.location.latitude}, ${entry.location.longitude}, ${((new Date(entry.versiontime).getTime() - new Date('01.01.2001').getTime()) / 1000)})`
                    }
                    db.transaction((tx) => {
                      tx.executeSql(queryTwo, [], (tx, results) => {
                        console.log(results)
                      },
                      function(e) {
                          console.log("ERROR: " + e.message)
                        })
                    })
                })
              })
            .done()
          }
      }).done((versiontime) => console.log("DONE DONE DONE"))
    })
  },

  updatePetrolCosts: function() {
      let len = this.state.markers.length
      let list = ""
      let chunk = 40 
      
      for(let i = 0; i < len; i++) {
        list += `${this.state.markers[i].serverid},`
          
        if((len < chunk && i == len-1) || (i > 0 && i % chunk  == 0) || (i == len-1)) {
          list = list.substr(0, list.length-1)

          let url = `https://api.spritradarapp.de/v1/petrolstations/dynamic/${list}`
          console.log(url)
          let username = 'spr1.0.0'
          let password = 'zdBAF7jfvDwR2HrBYFjoxtEpcYfAdyhYi'
          const hash = new Buffer(`${username}:${password}`).toString('base64')
          fetch(url, {method: "GET", headers: {'Authorization': `Basic ${hash}`}})
          .then((response) => response.json())
          .then((responseData) => {
            if(responseData != undefined) {
              responseData.map(row => {
                let fuelPriceE5 = 0.00
                let fuelPriceE10 = 0.00
                let fuelPriceDiesel = 0.00
                if(row.fuelPrices != undefined) {
                  row.fuelPrices.map(price => {
                    if(price.type == 'E5')
                      fuelPriceE5 = price.fuelPrice
                    if(price.type == 'E10')
                      fuelPriceE10 = price.fuelPrice
                    if(price.type == 'DIESEL')
                      fuelPriceDiesel = price.fuelPrice
                  })
                  let query = `UPDATE ZPETROLSTATION SET ZPRICEE5 = ${fuelPriceE5}, ZPRICEE10 = ${fuelPriceE10}, ZPRICEDIESEL = ${fuelPriceDiesel} WHERE ZSERVERID = '${row.petrolStationID}'`
                  db.transaction((tx) => {
                    tx.executeSql(query, [], (tx, results) => {
                    },function(e) {
                      console.log("ERROR: " + e.message)
                    })
                  })
                }
              })
            }
          })
          .done(() => { this.loadPetrolStations(), list = "" })
        }
      }
  },

  loadPetrolStations: async function() {
        var query = `SELECT * FROM ZPETROLSTATION WHERE (ZLOCATIONLATITUDE BETWEEN ${this.state.region.latitude - this.state.region.latitudeDelta / 2} AND
                     ${this.state.region.latitude + this.state.region.latitudeDelta / 2}) AND
                     (ZLOCATIONLONGITUDE BETWEEN ${this.state.region.longitude - this.state.region.longitudeDelta / 2} AND
                     ${this.state.region.longitude + this.state.region.longitudeDelta / 2})`.toString()
                     
        await db.transaction((tx, tmp) => {
          tx.executeSql(query, [], (tx, results, tmp) => {
            let that = this
            that.state.markers = []
            for (let i = 0; i < results.rows.length; i++) {
              let row = results.rows.item(i)
              let limit = 0.23
              if(row.ZLOCATIONLATITUDE < this.state.region.latitude + limit &&
                  row.ZLOCATIONLATITUDE > this.state.region.latitude - limit &&
                  row.ZLOCATIONLONGITUDE < this.state.region.longitude + limit * ASPECT_RATIO &&
                  row.ZLOCATIONLONGITUDE > this.state.region.longitude - limit * ASPECT_RATIO ) {
                that.state.markers.push({
                  coordinate: {latitude: row.ZLOCATIONLATITUDE, longitude: row.ZLOCATIONLONGITUDE},
                  key: row.Z_PK,
                  serverid: row.ZSERVERID,
                  info: {
                    pricee10: row.ZPRICEE10,
                    pricediesel: row.ZPRISEDIESEL,
                    pricee5: row.ZPRICEE5,
                    brand: row.ZBRAND,
                    street: row.ZSTREET,
                    housenumber: row.ZHOUSENUMBER,
                    postcode: row.ZPOSTCODE,
                    place: row.ZPLACE,
                    cluster: 0,
                    selected: false,
                  },
                })
              }
            }
            that.state.dataSource = that.state.dataSource.cloneWithRows(that.state.markers)
            this.setState(that)
          })
        }).done(() => {
            this.findCheapestMarker(),
            console.log("Updated markers from sqlite")
          })
  },

  getPriceFromSelectedType: function(marker) {
    let amount = 0.00
    switch(this.state.selectedGasType) {
      case 'E10':
        amount = marker.info.pricee10
      case 'E5':
        amount = marker.info.pricee5
        break
      case 'Diesel':
        amount = marker.info.pricediesel
      default:
        amount = 0.00
    }
    if(amount == null)
      amount = 9.99

    return amount
  },

  findCheapestMarker: function(){
    if(!this.state.userSelectedMarker) {
      let cheapest = undefined
      let lat = undefined
      let lon = undefined

      this.state.markers.map(marker => {
        let amount = this.getPriceFromSelectedType(marker)
        if(cheapest > 0 && cheapest < amount) {

        } else {
          cheapest = amount
          lat = marker.coordinate.latitude
          lon = marker.coordinate.longitude
        }
      })
      
      if(cheapest != undefined && lat != undefined  && lon != undefined) {
        if(this.state.selectedMarkerCost != undefined && this.state.selectedMarkerCost > cheapest) {
          PushNotification.localNotification({
              /* Android Only Properties */
              id: 0, // (optional) default: Autogenerated Unique ID
              title: "My Notification Title", // (optional)
              ticker: "My Notification Ticker", // (optional)
              largeIcon: "ic_launcher", // (optional) default: "ic_launcher"
              smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher"

              /* iOS and Android properties */
              message: `Found new cheapest marker: ${cheapest}` // (required)
          })
        }
        
        PushNotification.setApplicationIconBadgeNumber(cheapest*1000),
        this.setState({
            polyline: [],
            selectedMarkerCost: cheapest,
            selectedMarkerCoordinate: {latitude: lat, longitude: lon},
        })
      }
    }
  },

  getRouteForCoordinates: function(lat, lon, userSelectedMarker = false) {
    if(this.state.lastPosition != undefined) {
      let header = new Headers()
      header.append('X-Yours-client', 'www.formigas.de')
      let url = "http://www.yournavigation.org/api/1.0/gosmore.php?format=geojson&fast=0&flat=" + this.state.lastPosition.coords.latitude + "&flon=" + this.state.lastPosition.coords.longitude + "&tlat=" + lat.toString() + "&tlon=" + lon.toString()
        fetch(url, {method: "GET", headers: header})
        .then((response) => response.json())
        .then((responseData) => {
            let tmp = responseData.coordinates.toString().split(',')
            let route = []
            for(var i = 0; i < tmp.length; i = i+2) {
              route.push({latitude: Number(tmp[i+1]), longitude: Number(tmp[i])})
            }
            this.setState({
              polyline: route,
              distance: responseData.properties.distance,
              traveltime: responseData.properties.traveltime,
              userSelectedMarker: userSelectedMarker,
              selectedMarkerCoordinate: {latitude: lat, longitude: lon},
            })
          })
        .done()
    }
  },

  onMapPress(e){
    this.onMarkerDeselect(null)
  },

  onMarkerPress(e) {
    if(e.nativeEvent.coordinate != undefined) {
      this.onMarkerSelect(e)
    }
  },

  onMarkerSelect(e) {
      this.getRouteForCoordinates(e.nativeEvent.coordinate.latitude, e.nativeEvent.coordinate.longitude, true)
  },

  onMarkerDeselect(e) {
    this.setState({
      polyline: [],
      selectedMarkerCoordinate: {latitude: undefined, longitude: undefined},
      userSelectedMarker: false,
    })
    this.findCheapestMarker()
  },

  renderRow(rowData, sectionID, rowID) {
      return (
      <View style={{flexDirection: 'row'}}>
        <View style={{flexDirection: 'column', margin: 2}}> 
          <Text style={{color: 'red'}}>{rowData.info.brand}</Text>
          <Text>{this.getPriceFromSelectedType(rowData)}</Text>
        </View>
        <View style={{flexDirection: 'column', margin: 2}}>
          <View style={{flexDirection: 'row'}}>
            <Text>{rowData.info.street}</Text><Text> {rowData.info.housenumber}</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Text>{rowData.info.postcode}</Text><Text> {rowData.info.place}</Text>
          </View>
        </View>
        <View>
          <Text>{ (this.state.distance != undefined ? Number(this.state.distance).toFixed(2) : 0)}KM</Text>
        </View>
      </View>
      )
  },

  jumtToUserLocation() {
    if(this.state.lastPosition != undefined) {
      this.refs.mapView.animateToRegion({
          latitude: this.state.lastPosition.coords.latitude,
          longitude: this.state.lastPosition.coords.longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        }, 500)
    }
  },

  log() {
      console.log("########## Log ###########")
      console.log(this.state.polyline)
      if(this.state.lastPosition != undefined)
        console.log(`CurrentPosition: ${this.state.lastPosition.coords.latitude} / ${this.state.lastPosition.coords.longitude}`)
      console.log(`SelectedMarkerCoordinate: ${this.state.selectedMarkerCoordinate.latitude}, ${this.state.selectedMarkerCoordinate.longitude}`)
      console.log(`Distance: ${this.state.distance}`)
      console.log(`Traveltime: ${Math.ceil(Number(this.state.traveltime)/60)}`)
      console.log("##########################")
  },
  render() {
    return (
    <View style={styles.app}>
      <View style={styles.toolbarContainer}>
        <View style={styles.toolbar}>
          <TouchableHighlight underlayColor={'rgba(0,0,0,0)'} onPress={this.updatePetrolCosts}>
            <Image style={styles.toolbarButton} source={require('./images/settings.png')}
            />
          </TouchableHighlight>
          <TextInput style={styles.toolbarSearch} placeholder={'Suche oder Route'} /* onChangeText={(text) => this.setState({text})} value="Test" */ />
          <TouchableHighlight underlayColor={'rgba(0,0,0,0)'} onPress={this.jumtToUserLocation}>
            <Image style={styles.toolbarButton} source={require('./images/location.png')}
            />
          </TouchableHighlight>
        </View>
      </View>
      <MapView
        ref="mapView"
        style={styles.mapView}
        showsUserLocation={true}
        initialRegion={this.state.region}
        onMarkerPress={this.onMarkerPress}
        onRegionChangeComplete={this.onRegionChangeComplete}
        onPress={this.onMapPress}
        onMarkerSelect={this.onMarkerSelect}
        onMarkerDeselect={this.onMarkerDeselect} >
        <MapView.Polyline
          coordinates={this.state.polyline}
          strokeColor="rgba(0,0,200,0.8)"
          strokeWidth={3}
        />
        {this.state.markers.map(marker => (
          <MapView.Marker
            centerOffset={{x: 0, y:-30}}
            key={marker.key}
            coordinate={marker.coordinate}>
              <PetrolStationMarker amount={this.getPriceFromSelectedType(marker)} info={marker.info} selected={this.markerIsSelected(marker)} />
          </MapView.Marker>
        ))}
      </MapView>
      <Animated.View style={[styles.information, {transform: [{ translateY: this.state.pan.y }]}]} >
        <View ref="listBar" style={styles.listBar}
         {...this._panResponder.panHandlers} 
          >
          <Text  style={styles.listBarText} >=</Text>
        </View>
        <ListView
          ref="list"
          style={styles.list}
          dataSource={this.state.dataSource}
          renderRow={this.renderRow}
          renderSeparator={(sectionID, rowID) => <View key={`${sectionID}-${rowID}`} style={styles.separator} />} />
      </Animated.View>
    </View>
    )
  },
})

var styles = StyleSheet.create({
  app: {
    flex: 10,
    paddingTop: 35,
    backgroundColor: 'rgb(248,246,243)',
  },
  toolbarContainer: {
    paddingBottom:5,
  },
  toolbar: {
    flexDirection: 'row',
  },
  toolbarButton: {
    width:20,
    height:20,
    marginTop:2,
    marginLeft:10,
    marginRight:10,
  },
  toolbarSearch: {
    flex: 1,
    backgroundColor: '#fff',
    height:25,
    textAlign: 'center',
    borderRadius: 5,
  },
  mapView: {
    flex: 10,
  },
  information: {
    position: 'absolute',
    left:0,
    right:0,
    top: height -55,
    height: 700,
    backgroundColor: 'rgb(248,246,243)',
  },
  list: {
    height: 120, 
  },
  listBar: {
    backgroundColor: 'darkgray',
  },
  listBarText: {
    textAlign: 'center', 
  },
  separator: {
    height: 1,
    backgroundColor: '#CCCCCC',
  },
})

module.exports = App
