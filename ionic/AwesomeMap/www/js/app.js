var db = null;

angular.module('awesome-map', ['ionic', 'ngCordova', 'LocalStorageModule', 'awesome-map.controllers'])

  .run(function ($ionicPlatform, $cordovaSQLite) {
    $ionicPlatform.ready(function () {
      if (window.cordova && window.cordova.plugins.Keyboard) {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

        // Don't remove this line unless you know what you are doing. It stops the viewport
        // from snapping when text inputs are focused. Ionic handles this internally for
        // a much nicer keyboard experience.
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }

      if (window.plugins) {
        var location = ionic.Platform.isIOS() ? 2 : 0;
        window.plugins.sqlDB.remove("database.db", location);
        window.plugins.sqlDB.copy("database.db", location, function () {
          console.log("Success");
        }, function (error) {
          console.error(error);
          console.error("Error");
        });
      }

      db = $cordovaSQLite.openDB({ name: 'database.db', iosDatabaseLocation: 'default' });
      $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS tasks (id integer primary key, title text, content text, completed boolean)");

    });
  })

  .config(function (localStorageServiceProvider) {
    localStorageServiceProvider.setPrefix('awesome-map');
  })

  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('todo', {
        url: '/todo',
        templateUrl: 'templates/todo.html',
        controller: 'TodoCtrl'
      })
      .state('map', {
        url: '/map',
        templateUrl: 'templates/map.html',
        controller: 'MapCtrl'
      })

    $urlRouterProvider.otherwise('/map');
  })


