angular.module('awesome-map.controllers', ['awesome-map.services'])

    .controller('MapCtrl', function ($scope, $ionicPlatform, $state, $http, $ionicPopup, $cordovaGeolocation, $cordovaSQLite, ApiEndpoints) {
        $scope.map = null;
        $scope.pos = null;
        $scope.petrolStations = [];
        $scope.selectedPetrolStation = null;
        $scope.oldHeight = -318;
        $scope.listStyle = {
                'position' : 'absolute',
                'height' : '388px',
                'bottom' : $scope.oldHeight + 'px',
                'background-color' : 'white',
        };
        
        $scope.list = {
            'max-height' : '318px',
        }

        $scope.goTodo = function () {
            $state.go('todo');
        }
        
        $scope.onDrag = function (event) {
            var deltaY = event.gesture.deltaY;
            var newOffset = $scope.oldHeight + (-deltaY)
            if(newOffset > 0)
                newOffset = 0
            $scope.listStyle.bottom = newOffset + 'px';
            $scope.listStyle.transition = 'initial';
        }
        
        $scope.onRelease = function(event) {
            var draggedOffset = parseInt($scope.listStyle.bottom);
            var offset = 0;
        
            if(draggedOffset > -190) {
                offset = -20;
            }
            else if (draggedOffset <= -190) {
                offset = -318;
            }
            
            console.log("DraggedOffset: " + draggedOffset);
            console.log("Offset: " + offset)
            $scope.oldHeight = offset;
            $scope.listStyle.bottom = offset + 'px';
            $scope.listStyle.transition = 'bottom 0.3s cubic-bezier(.57,.07,.51,1.47)'
        }

        $scope.clickSettings = function () {
            $scope.updatePrices();
        }

        $scope.updatePrices = function () {
            var list = "";
            $scope.petrolStations.forEach(function (element) {
                var id = element.getId();
                list = list + id + ",";
            });
            list = list.substr(0, (list.length - 1));

            var username = 'spr1.0.0';
            var password = 'zdBAF7jfvDwR2HrBYFjoxtEpcYfAdyhYi';
            const hash = btoa(username + ":" + password);
            var header = {
                method: 'GET',
                headers: { 'Authorization': 'Basic ' + hash }
            }

            if (list.length > 0) {
                $http.get(ApiEndpoints.spritRadar + list, header).then(function (res) {
                    res.data.forEach(function (element) {
                        var priceE5 = -1;
                        var priceE10 = -1;
                        var priceDIESEL = -1;
                        var petrolStationID = element.petrolStationID;

                        if (element.fuelPrices) {
                            element.fuelPrices.forEach(function (priceElement) {
                                switch (priceElement.type) {
                                    case 'E5':
                                        priceE5 = priceElement.fuelPrice;
                                        break;
                                    case 'E10':
                                        priceE10 = priceElement.fuelPrice;
                                        break;
                                    case 'DIESEL':
                                        priceDIESEL = priceElement.fuelPrice;
                                        break;
                                    default:
                                        break;
                                }
                            });
                        }

                        if (priceE5 > 0) {
                            var query = "UPDATE ZPETROLSTATION SET ZPRICEE5=? WHERE ZSERVERID = ?";
                            $cordovaSQLite.execute(db, query, [priceE5, petrolStationID]).then(function (res) {
                                console.log("E5 SUCCESS");
                            }, function (err) {
                                console.log("E5 ERROR");
                            });
                        }

                        if (priceE10 > 0) {
                            var query = "UPDATE ZPETROLSTATION SET ZPRICEE10=? WHERE ZSERVERID = ?";
                            $cordovaSQLite.execute(db, query, [priceE10, petrolStationID]).then(function (res) {
                                console.log("E10 SUCCESS");
                            }, function (err) {
                                console.log("E10 ERROR");
                            });
                        }

                        if (priceDIESEL > 0) {
                            var query = "UPDATE ZPETROLSTATION SET ZPRICEDIESEL=? WHERE ZSERVERID = ?";
                            $cordovaSQLite.execute(db, query, [priceDIESEL, petrolStationID]).then(function (res) {
                                console.log("DIESEL SUCCESS");
                            }, function (err) {
                                console.log("DIESEL ERROR");
                            });
                        }
                    });
                    $scope.loadMarker();
                }, function (err) {
                    console.log(JSON.stringify(err));
                })
            }
        }

        $scope.loadMarker = function () {
            if ($scope.map.getZoom() > 10) {
                var selectedPetrolStation = $scope.selectedPetrolStation;
                var tmp = $scope.petrolStations.slice();
                $scope.petrolStations = [];
                var bounds = $scope.map.getBounds();
                var query = "SELECT * FROM ZPETROLSTATION WHERE (ZLOCATIONLATITUDE BETWEEN ? AND ?) AND (ZLOCATIONLONGITUDE BETWEEN ? AND ?)";
                var offset = 0.02;
                $cordovaSQLite.execute(db, query, [bounds.getSouthWest().lat() - offset, bounds.getNorthEast().lat()  + offset, bounds.getSouthWest().lng()  - offset, bounds.getNorthEast().lng()  + offset]).then(function (res) {
                    for (var i = 0; i < res.rows.length; i++) {
                    console.log(i);
                        var item = res.rows.item(i);
                        var markerOptions = {
                            position: new google.maps.LatLng(item.ZLOCATIONLATITUDE, item.ZLOCATIONLONGITUDE),
                            map: $scope.map,
                            title: item.ZPRICEE10,
                        }
                        var marker = new PetrolStationMarker(
                            new google.maps.LatLng(item.ZLOCATIONLATITUDE, item.ZLOCATIONLONGITUDE),
                            $scope.map,
                            item.ZSERVERID,
                            {
                                brand: item.ZBRAND,
                                price: item.ZPRICEE5,
                                selected: (selectedPetrolStation && item.ZLOCATIONLATITUDE == selectedPetrolStation.lat() && item.ZLOCATIONLONGITUDE == selectedPetrolStation.lng())
                            }
                        );

                        google.maps.event.addListener(marker, 'click', function (pos) {
                            $scope.selectedPetrolStation = pos;
                            $scope.calculateRoute(pos);
                            $scope.loadMarker();
                        })

                        $scope.petrolStations.push(marker);
                    }
                }, function (err) {
                    console.error(err);
                }).then(function () {
                    tmp.forEach(function (element) {
                        element.setMap(null);
                    }, this);
                })
            } else {
                $scope.petrolStations.forEach(function (element) {
                    element.setMap(null);
                }, this);
                $scope.petrolStations = [];
                $scope.selectedPetrolStation = null;
                $scope.route.setMap(null);
            }
        }

        $scope.addUserPosition = function () {
            var circleOptions = {
                center: $scope.pos,
                radius: 100,
                map: $scope.map,
                fillColor: '#0000FF',
                fillOpacity: 0.6,
                strokeColor: '#0000FF',
                strokeOpacity: 1.0
            };

            var circle = new google.maps.Circle(circleOptions);
            $scope.map.addListener("idle", function () {
                circle.setRadius(($scope.map.getBounds().getNorthEast().lat() - $scope.map.getBounds().getSouthWest().lat()) * 800);
            })
        }

        $scope.calculateRoute = function (toLatLng) {
            var url = ApiEndpoints.yourNavigation + "gosmore.php?format=geojson&fast=0&flat=" + $scope.pos.lat() +
                "&flon=" + $scope.pos.lng() + "&tlat=" + toLatLng.lat() + "&tlon=" + toLatLng.lng();

            var options = {
                headers: {
                    'X-Yours-client': 'www.formigas.de',
                },
            };

            $http.get(url, options).then(function (res) {
                var routeCoords = []
                res.data.coordinates.forEach(function (element) {
                    routeCoords.push(new google.maps.LatLng(element[1], element[0]));
                });

                var routeOptions = {
                    path: routeCoords,
                    geodesic: true,
                    strokeColor: '#0000FF',
                    strokeOpacity: 1.0,
                    strokeWeight: 2,
                }

                if ($scope.route !== undefined)
                    $scope.route.setMap(null)

                $scope.route = new google.maps.Polyline(routeOptions)
                $scope.route.setMap($scope.map)
            }, function (err) {
                console.error(JSON.stringify(err));
            });
        }

        $scope.initMap = function (position) {
            $scope.pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

            var mapOptions = {
                center: $scope.pos,
                zoom: 14,
                mapTypeControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{ featureType: "poi", elementType: "labels", stylers: [{ visibility: "off" }]},
                         { featureType: "transit", stylers: [{ "visibility": "off" }]}]
            };

            $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);
            $scope.addUserPosition();

            $scope.map.addListener("idle", function () {
                $scope.loadMarker();
            })

            $scope.map.addListener("click", function (evt) {
                if (evt.latLng) {
                    if ($scope.route) {
                        $scope.route.setMap(null);
                    }
                    $scope.selectedPetrolStation = null;
                    $scope.loadMarker();
                }
            })
            
        }


        $scope.init = function () {
            var options = { timeout: 10000, enableHighAccuracy: true };

            $cordovaGeolocation.getCurrentPosition(options).then(function (position) {
                $scope.initMap(position);
            }, function () {
                $scope.initMap({
                    coords: { latitude: 47.672152, longitude: 9.154585 }
                });
            });
        }

        $ionicPlatform.ready(function () {
            $scope.init();
        })
    })

    .controller('TodoCtrl', function ($scope, $state, $ionicModal, $ionicPlatform, localStorageService, $cordovaSQLite) {
        var taskData = 'task';
        $scope.tasks = [];
        $scope.task = {};

        $scope.goMap = function () {
            $state.go('map');
        }

        $ionicModal.fromTemplateUrl('new-task-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.newTaskModal = modal;
        });

        $scope.removeTaskSQL = function (index) {
            var query = "DELETE FROM tasks WHERE id = ?";
            console.log($scope.tasks[index]);
            $cordovaSQLite.execute(db, query, [$scope.tasks[index].id]).then(function (res) {
                console.log("DELETED ID _> " + $scope.tasks[index].id);
            }, function (err) {
                console.error(err);
            });
            $scope.task = {};
            $scope.getTasksSQL();
        }

        $scope.completeTaskSQL = function (index) {
            if (index !== -1) {
                var query = "UPDATE tasks SET completed=? WHERE id = ?";
                $cordovaSQLite.execute(db, query, [$scope.tasks[index].completed ? 1 : 0, $scope.tasks[index].id]).then(function (res) {
                    console.log("UPDATED ID _> " + res.insertId);
                }, function (err) {
                    console.error(JSON.stringify(err));
                });

                $scope.getTasksSQL();
            }
        }

        $scope.openTaskModal = function () {
            $scope.newTaskModal.show();
        };

        $scope.closeTaskModal = function () {
            $scope.newTaskModal.hide();
        };

        $scope.createTaskSQL = function () {
            var query = "INSERT INTO tasks (title, content, completed) VALUES(?,?,?)";
            $cordovaSQLite.execute(db, query, [$scope.task.title, $scope.task.content, $scope.task.completed ? 1 : 0]).then(function (res) {
                console.log("INSERT ID _> " + res.insertId);
            }, function (err) {
                console.error(err);
            });
            $scope.task = {};
            $scope.newTaskModal.hide();
            $scope.getTasksSQL();
        }

        $scope.getTasksSQL = function () {
            var query = "SELECT id, title, content, completed FROM tasks";
            $cordovaSQLite.execute(db, query).then(function (res) {
                $scope.tasks = [];
                if (res.rows.length > 0) {
                    for (var i = 0; i < res.rows.length; i++) {
                        console.log(JSON.stringify(res.rows.item(i)));
                        $scope.tasks.push(res.rows.item(i));
                        $scope.tasks[i].completed = res.rows.item(i).completed == 1;
                    }
                } else {
                    console.log("No results found");
                }
            }, function (err) {
                console.error(err);
            })
        }

        $ionicPlatform.ready(function () {
            $scope.getTasksSQL();
        });
    })