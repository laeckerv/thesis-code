angular.module('awesome-map.constants', [])

.constant('ApiEndpoints', {
    spritRadar: 'https://api.spritradarapp.de/v1/petrolstations/dynamic/',
    yourNavigation: 'http://www.yournavigation.org/api/1.0/' 
 })
