function PetrolStationMarker(latlng, map, serverId, args) {
    this.latlng = latlng;
    this.serverId = serverId;
    this.args = args;
    this.setMap(map);
    this.selected = false;
}

PetrolStationMarker.prototype = new google.maps.OverlayView();

PetrolStationMarker.prototype.getId = function () {
    return this.serverId;
}

PetrolStationMarker.prototype.draw = function () {

    var self = this;

    var div = this.div;
    var arrow = this.arrow;

    if (!div) {

        div = this.div = document.createElement('div');


        if (typeof (self.args.selected) !== 'undefined') {
            self.selected = self.args.selected;
        }

        div.className = 'marker';

        div.style.position = 'absolute';
        div.style.cursor = 'pointer';
        div.style.width = '70px';
        div.style.height = '50px';
        div.style.background = 'white';
        div.style.borderRadius = '5px';
        div.style.borderWidth = '1px';
        div.style.borderStyle = 'solid';
        div.style.borderColor = self.selected ? 'red' : 'black';
        div.style.padding = '3px';

        if (typeof (self.args.marker_id) !== 'undefined') {
            div.dataset.marker_id = self.args.marker_id;
        }
        
        if (typeof (self.args.brand) !== 'undefined') {
            var element = document.createElement('div');
            var text = document.createTextNode(self.args.brand);
            element.style.fontSize = '12px';
            element.style.color = 'red';
            element.style.textTransform = 'uppercase';
            element.style.textOverflow = 'ellipsis';
            element.style.whiteSpace = 'nowrap';
            element.style.overflow = 'hidden';
            element.appendChild(text);
            div.appendChild(element);
        }

        if (typeof (self.args.price) !== 'undefined' && self.args.price !== null) {
            var element = document.createElement('div');
            var text = document.createTextNode(self.args.price.toString().substring(0, 4));
            element.style.fontSize = '20px';
            element.style.color = 'black';
            element.appendChild(text);
            var elementSmall = document.createElement('div');
            var textSmall = document.createTextNode(self.args.price.toString().substring(4, 5));
            elementSmall.style.fontSize = '10px';
            elementSmall.style.marginTop = '-22px';
            elementSmall.style.marginLeft = '39px';
            elementSmall.appendChild(textSmall);
            div.appendChild(element);
            div.appendChild(elementSmall);
        }
        

        arrow = this.arrow = document.createElement('div');
        arrow.style.borderLeft = '50px solid transparent'
        arrow.style.borderRight = '50px solid transparent'
        arrow.style.borderTop = '100px solid black'
        arrow.style.borderTopColor = self.selected ? 'red' : 'black';
        arrow.style.transform = 'scale(0.15)';
        arrow.style.marginTop = '-35px';
        arrow.style.marginLeft = '-20px';
        div.appendChild(arrow);

        google.maps.event.addDomListener(div, "click", function (event) {
            setTimeout(function () {
                google.maps.event.trigger(self, "click", self.getPosition());
                div.style.borderColor = 'red';
                arrow.style.borderTopColor = 'red';
            }, 50);
        });
        
        var panes = this.getPanes();
        panes.overlayImage.appendChild(div);
    }

    var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

    if (point) {
        div.style.left = (point.x - 35) + 'px';
        div.style.top = (point.y - 60) + 'px';
    }
};

PetrolStationMarker.prototype.remove = function () {
    if (this.div) {
        this.div.parentNode.removeChild(this.div);
        this.div = null;
    }
};

PetrolStationMarker.prototype.getPosition = function () {
    return this.latlng;
};